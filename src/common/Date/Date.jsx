import React from 'react';
import ProtoTypes from 'prop-types'

const DateFormat = ({
  data
}) => {
  const formattedDate = new Date(data).toLocaleDateString('en-IN', { month: "short", year: "numeric", day: "numeric" });
  return(<span>
    {formattedDate.split(' ').join('-')}
    </span>)
};

export default DateFormat;
