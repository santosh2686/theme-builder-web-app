import React, { memo } from 'react';
import { string } from 'prop-types';
import ClassNames from 'classnames';
import {computeValue} from '@utils';
import { MapCssModules } from '@utils';

const TableCell = ({ item, column, rowIndex, expand }) => {
  const { custom, map, classes } = column;
  const eltClass = ClassNames('pad-tb-10 pad-lr-15 bor-b-gray-light', {
    [classes]: classes,
    'expand': expand
  });
  if(custom) {
    return (
      <td
        styleName={eltClass}>
        {custom(item, rowIndex)}
      </td>
    )
  }

  return (
    <td
      styleName={eltClass}>
      {computeValue(item, map)}
    </td>
  )
};

export default memo(MapCssModules(TableCell));