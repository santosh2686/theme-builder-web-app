import React, { memo } from 'react';
import { string, node } from 'prop-types';
import ClassNames from 'classnames';
import { MapCssModules } from '@utils';

const HeaderCell = ({ children, classes }) => {
  const eltClass = ClassNames('font-12 text-left uppercase pad-tb-10 pad-lr-15 bor-b-gray-light bor-width-2', {
    [classes]: classes,
  });
  return (
    <th styleName={eltClass}>
      {children}
    </th>
  )
};

HeaderCell.propTypes = {
  children: node.isRequired,
  classes: string
};

HeaderCell.defaultProps = {
  classes: ''
};

export default memo(MapCssModules(HeaderCell))
