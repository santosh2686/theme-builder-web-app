import React, { memo } from 'react'
import { string, oneOf, object } from 'prop-types';
import ClassNames from 'classnames';
import { MapCssModules } from '@utils';

const Icon = ({ tag: Tag, name, classes, color, size, iconScale, spin, attributes }) => {
  const eltClass = ClassNames('vertical-middle  ', {
    [`color-${color}`]: color,
    [`font-${size}`]: size,
    [classes]: classes
  });
  const iconClass = ClassNames('fa', {
    [`fa-${name}`]: name,
    [`fa-${iconScale}`]: iconScale,
    'fa-spin': spin
  });
  return (
    <Tag styleName={eltClass} { ...attributes }>
      <i className={iconClass} />
    </Tag>
  )
};

Icon.propTypes = {
  name: string.isRequired,
  classes: string,
  tag: string,
  color: string,
  size: oneOf(['8', '14', '22', '30']),
  iconScale: oneOf(['', '2x', '3x', '4x', '5x', '6x', '7x', '8x', '9x', '10x']),
  attributes: object
};

Icon.defaultProps = {
  classes: '',
  tag: 'span',
  color: 'gray',
  size: '14',
  iconScale: '',
  attributes: {}
};

export default memo(MapCssModules(Icon))
