import React, { memo } from 'react'
import { NavLink } from 'react-router-dom';
import { any, bool, func, string } from 'prop-types';
import ClassNames from 'classnames'
import { MapCssModules } from '@utils'

const Anchor = ({
  children,
  href,
  color,
  hoverColor,
  attributes,
  asButton,
  block,
  asLink,
  type,
  clickHandler,
  noUnderline,
  title,
  classes
}) => {
  const eltClass = ClassNames({
    [`color-${color}`]: color,
    [`hover-color-${hoverColor}`]: hoverColor,
    'bor-none pad-0 text-decoration-none bg-none': asButton,
    'text-decoration-none': noUnderline,
    'show-block': block,
    [classes]: classes
  });

  if (asButton) {
    return (
      <button
        type={type}
        onClick={clickHandler}
        styleName={eltClass}
        {...attributes}
      >
        {children}
      </button>
    )
  }

  if (asLink) {
    return (
      <NavLink
        to={href}
        {...attributes}
        styleName={eltClass}
      >
        {children}
      </NavLink>
    )
  }

  return (
    <a
      href={href}
      title={title}
      {...attributes}
      styleName={eltClass}>
      {children}
    </a>
  )
};

Anchor.propTypes = {
  children: any.isRequired,
  href: string,
  classes: string,
  color: string,
  hoverColor: string,
  title: string,
  asButton: bool,
  type: string,
  asLink: bool,
  block: bool,
  noUnderline: bool,
  clickHandler: func,
};

Anchor.defaultProps = {
  classes: '',
  href: '/',
  color: 'primary',
  hoverColor: 'primary-light',
  title: '',
  asButton: false,
  type: 'button',
  asLink: false,
  block: false,
  noUnderline: false,
  clickHandler: () => {
  },
};

export default memo(MapCssModules(Anchor))
