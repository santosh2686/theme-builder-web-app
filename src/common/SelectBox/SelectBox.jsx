import React, { PureComponent } from 'react'
import { arrayOf, bool, func, object, string } from 'prop-types'
import ClassNames from 'classnames';

import { MapCssModules, computeValue } from '@utils'

class SelectBox extends PureComponent {

  changeHandler = ({ target: { value } }) => {
    const { changeHandler, name } = this.props;
    changeHandler({ [name]: value })
  };

  render() {
    const { label, options, value, keyMap, valueMap, placeHolder, required,
      disabled, attributes, classes } = this.props;
    const eltClass = ClassNames('relative select-box', {
      [classes]: classes,
      'disabled': disabled,
    });
    return (
      <div styleName={eltClass}>
        {label&& <label styleName="show-block pad-b-5">
          {label}
          {required && <span styleName="color-gray">*</span>}
        </label> }
        <select
          styleName="width-100 bg-white pad-tb-5 pad-l-10 pad-r-25 font-14 bor-gray-light input-control capitalize"
          value={value}
          disabled={disabled}
          required={required}
          onChange={this.changeHandler}
          {...attributes}>
          {placeHolder && <option value="">{placeHolder}</option>}
          {options.map((item, index) => {
            if (keyMap && valueMap) {
              const value = computeValue(item, keyMap);
              return (<option key={value} value={value}>{item[valueMap]}</option>)
            } else {
              return (<option key={index} value={item}>{item}</option>)
            }
          })}
        </select>
      </div>
    )
  }
}

SelectBox.propTypes = {
  label: string,
  options: arrayOf(object),
  value: string,
  keyMap: string,
  valueMap: string,
  placeHolder: string,
  disabled: bool,
  required: bool,
  classes: string,
  changeHandler: func
};

SelectBox.defaultProps = {
  label: '',
  options: [],
  value: '',
  keyMap: 'key',
  valueMap: 'value',
  placeHolder: '',
  disabled: false,
  required: false,
  classes: '',
  changeHandler: () => {},
};

export default MapCssModules(SelectBox)
