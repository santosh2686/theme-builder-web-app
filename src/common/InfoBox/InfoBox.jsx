import React, { memo } from 'react'
import { bool, string, number } from 'prop-types';
import ClassNames from 'classnames';
import { MapCssModules } from '@utils'

import Layout from '../Layout/Layout.jsx';
import Icon from '../Icon/Icon.jsx';
import Text from '../Text/Text.jsx';
import Currency from '../Currency/Currency.jsx';

const InfoBox = ({ iconName, label, value, isCurrency, classes }) => {
    const eltClass = ClassNames({
        [classes]: classes
    });
    return (
        <Layout
            flex={{ align: 'center', justify: 'space-between' }}
            bgColor="white"
            pad="20"
            classes={eltClass}
        >
            <Icon name={iconName} iconScale="4x" color="success" />
            <div styleName="text-right">
                <Text size="24">
                    {isCurrency ? (
                        <Currency data={value} />
                    ) : value}
                    </Text>
                <Text tag="p" size="18" color="gray">{label}</Text>
            </div>
        </Layout>
    )
};

InfoBox.propTypes = {
    iconName: string.isRequired,
    label: string.isRequired,
    value: number.isRequired,
    isCurrency: bool,
    classes: string
};

InfoBox.defaultProps = {
    isCurrency: false,
    classes: ''
};

export default memo(MapCssModules(InfoBox))
