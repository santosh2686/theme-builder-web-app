import React, { memo } from 'react'
import { NavLink } from 'react-router-dom';
import { any, bool, func, string } from 'prop-types';
import ClassNames from 'classnames';

import { MapCssModules } from '@utils';

import Icon from '../Icon/Icon';

const Button = ({
  children,
  type,
  category,
  size,
  disabled,
  block,
  rounded,
  classes,
  outline,
  clickHandler,
  asAnchor,
  asLink,
  loading,
  href
}) => {
  const eltClass = ClassNames('btn no-wrap no-select', {
    [`bg-${category} hover-bg-${category}-light`]: category && !outline,
    'color-white': (category !== 'default' && !outline),
    'color-black': category === 'default',
    [`btn-${size}`]: size,
    'show-block width-100 mar-l-0': block,
    'show-inline-block': !block,
    'disabled': disabled,
    'btn-rounded': rounded,
    'bor-none': !outline,
    [`bg-white color-${category} bor-${category}`]: !loading && outline,
    'text-center text-decoration-none': asAnchor || asLink,
    'relative overflow-hidden pointer-events-none': loading,
    [`color-white bor-${category}`]: loading && outline,
    [classes]: classes,
  });

  const loadingClass = ClassNames('absolute flex align-center flex-center btn-loading', {
    [`bg-${category} hover-bg-${category}-light`]: !outline,
    [`color-${category}`]: outline,
  });

  if (asLink) {
    return (
      <NavLink
        to={href}
        styleName={eltClass}>
        {children}
      </NavLink>
    )
  }

  if (asAnchor) {
    return (
      <a
        href={href}
        styleName={eltClass}>
        {children}
      </a>
    )
  }

  return (
    <button
      type={type}
      styleName={eltClass}
      disabled={disabled}
      onClick={clickHandler}>
      {loading && (
        <Icon
          name="spinner"
          color="white"
          size="16"
          spin
          classes={loadingClass}
        />
      )}
      {children}
    </button>
  )
};

Button.propTypes = {
  children: any.isRequired,
  type: string,
  category: string,
  disabled: bool,
  loading: bool,
  classes: string,
  href: string,
  clickHandler: func
};

Button.defaultProps = {
  type: 'button',
  category: 'primary',
  disabled: false,
  loading: false,
  classes: null,
  href: '/',
  clickHandler: () => { }
};

export default memo(MapCssModules(Button))
