import React, { PureComponent } from 'react';
import { string, bool } from 'prop-types';
import ReactDatePicker from 'react-datepicker';
import ClassNames from 'classnames';
import { MapCssModules } from '@utils';
import { toISOString } from '@utils/date';

import "react-datepicker/dist/react-datepicker.css";

class DatePicker extends PureComponent {
    dateChangeHandler = date => {
        const { name, changeHandler } = this.props;
        changeHandler({
            [name]: toISOString(date)
        })
    }

    render() {
        const {
            label,
            required,
            selected,
            minDate,
            maxDate,
            placeholder,
            format,
            classes
        } = this.props;
        const eltClass = ClassNames({
            [classes]: classes,
        });
        return (
            <div styleName={eltClass}>
                {label && <label styleName="show-block pad-b-5">
                    {label}
                    {required && <span styleName="color-gray">*</span>}
                </label>}
                <ReactDatePicker
                    dateFormat={format}
                    placeholderText={placeholder}
                    selected={selected ? new Date(selected) : new Date()}
                    onChange={this.dateChangeHandler}
                    dropdownMode="select"
                    minDate={minDate ? new Date(minDate) : ''}
                    maxDate={maxDate ? new Date(maxDate) : ''}
                    className="width-100 bg-white pad-tb-5 input-control
                    bor-gray-light pad-lr-10 color-gray"
                />
            </div>
        );
    }
}

DatePicker.propTypes = {
    label: string,
    required: bool,
    selected: string,
    minDate: string,
    maxDate: string,
    classes: string,
    placeholder: string,
    format: string
};
DatePicker.defaultProps = {
    label: '',
    required: false,
    selected: toISOString(new Date()),
    minDate: '',
    maxDate: '',
    classes: '',
    placeholder: 'Select Date',
    format: 'dd-MMM-yyyy'
};

export default MapCssModules(DatePicker)
