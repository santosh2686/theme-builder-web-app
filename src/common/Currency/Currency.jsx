import React, { memo } from 'react';
import { number, string, oneOfType } from 'prop-types'
import Icon from '../Icon/Icon.jsx';

const Currency = ({
  data
}) => {
  return(
    <span>
      <Icon name="inr" />&nbsp;
      {Number(data).toLocaleString('en-IN', { maximumFractionDigits : 2, minimumFractionDigits : 2 })}
    </span>)
};

Currency.propTypes = { 
  data: oneOfType([number, string])
}

Currency.defaultProps = {
  data: 0
}

export default memo(Currency);
