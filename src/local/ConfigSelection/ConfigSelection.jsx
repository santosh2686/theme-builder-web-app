import React, { PureComponent } from "react";

import { ConfigContext } from "@context";

import { SelectBox } from '@common';

class ConfigSelection extends PureComponent {
  computeOptions = (objectInput) => {
    return Object.keys(objectInput).reduce((acc, next) => {
      acc.push({
        key: next,
        value: next
      });
      return acc;
    },[])
  }

  render() {
    const { configKey, label, name, selected, changeHandler } = this.props;
    return (
      <ConfigContext.Consumer>
        {(context) => {
          const { config } = context;
          const { baseColors, brandColors } = config;
          const colorOptions = {
            ...baseColors,
            ...brandColors
          }
          const configValue = configKey === 'colors' ? colorOptions : config[configKey]
          const options = this.computeOptions(configValue)
          return (
            <SelectBox
              label={label}
              name={name}
              value={selected}
              options={options}
              changeHandler={changeHandler}
            />
          )
        }}
      </ConfigContext.Consumer>
    )
  }
}

export default ConfigSelection;
