import React, { Fragment, PureComponent } from 'react'

import { Button, Icon, Layout, Text } from '@common'
import ConfigWrapper from './ConfigWrapper'
import update from "immutability-helper"

class MultipleConfigurationModal extends PureComponent {
  state = {
    data: {}
  }

  componentDidMount() {
    const { data } = this.props
    this.setState({
      data
    })
  }

  changeHandler = (valueObj) => {
    this.setState(prevState => update(prevState, {
      data: valueObj
    }))
  }

  submitHandler = () => {
    const { data } = this.state
    const { submitHandler, closeModal } = this.props
    submitHandler(data)
    closeModal()
  }

  render() {
    const { category, configMapKey, closeModal } = this.props
    const { data } = this.state
    return (
      <Fragment>
        <Layout flex={{ align: 'center', justify: 'space-between' }} pad={{ tb: 5, lr: 10 }}>
          <Text tag="h4" classes="capitalize">
            {`${category} configuration`}
          </Text>
          <Button category="clear" clickHandler={closeModal}>
            <Icon name="close" />
          </Button>
        </Layout>
        <Layout
          pad={{ t: 15, l: 15, r: 15 }}
          border={{ tb: 'gray-light'}}
          bgColor="gray-light"
          classes="overflow-auto"
        >
          <ConfigWrapper
            data={data}
            configMapKey={configMapKey}
            changeHandler={this.changeHandler}
          />
        </Layout>
        <Layout classes="text-right" pad={{ tb: 5, lr: 10 }}>
          <Button clickHandler={this.submitHandler}>
            Submit
          </Button>
        </Layout>
      </Fragment>
    )
  }
}

export default MultipleConfigurationModal
