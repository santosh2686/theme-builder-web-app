import React, { PureComponent } from "react";
import update from "immutability-helper"

import { arrayToObject, objectToArray } from '@utils';

import { Panel } from '@common'

import AddRemoveItem from '../AddRemoveItem/AddRemoveItem.jsx';
import Multiple from './Multiple.jsx'

class MultipleConfigItem extends PureComponent {
  state = {
    isLoading: true,
    data: []
  }

  componentDidMount() {
    const { data } = this.props;
    const updatedData = objectToArray(data)
    this.setState({
      data: updatedData,
      isLoading: false,
    })
  }

  updateState = (valueObj) => {
    this.setState(prevState => update(prevState, {
      data: valueObj
    }), () => {
      const { data } = this.state;
      const { changeHandler, category } = this.props;
      changeHandler({
        [category]: {
          $set: arrayToObject(data)
        }
      })
    })
  }

  addHandler = () => {
    const { data } = this.state
    const { value } = data[0]
    this.updateState({
      $push: [
        {
          name: "",
          value,
        },
      ],
    });
  };

  removeHandler = (index) => {
    this.updateState({
      $splice: [[index, 1]],
    });
  };

  nameChangeHandler = (data, index) => {
    this.updateState({
      [index]: {
        $merge: data
      }
    })
  }

  submitHandler = (data, index) => {
    const valueObj = {
      [index]: {
        value: {
          $set: data
        }
      }
    }
    this.updateState(valueObj)
  }

  render() {
    const { data, isLoading } = this.state;
    const { category, configMapKey } = this.props;
    if (isLoading) {
      return null
    }
    return (
      <Panel title={category} classes="flex-1 mar-b-15 pad-b-15">
        <AddRemoveItem
          itemList={data}
          addHandler={this.addHandler}
          removeHandler={this.removeHandler}
        >
          <Multiple
            nameChangeHandler={this.nameChangeHandler}
            submitHandler={this.submitHandler}
            configMapKey={configMapKey}
          />
        </AddRemoveItem>
      </Panel>
    );
  }
}

export default MultipleConfigItem
