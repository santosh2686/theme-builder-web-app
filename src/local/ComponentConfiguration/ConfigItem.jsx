import React from 'react';

import { Row, Panel, Layout } from '@common';

import PropItem from './PropItem.jsx';

const ConfigItem = ({
  configMapKey,
  category,
  data,
  changeHandler
}) => {
  if (typeof data !== 'object') {
    return (
      <Layout bgColor="white" pad={{ t: 10 }}>
        <PropItem
          key={`${configMapKey}_${data}`}
          propName={category}
          category={category}
          value={data}
          changeHandler={changeHandler}
        />
      </Layout>
    )
  }
  const propKeys = Object.keys(data)
  return (
    <Panel title={category} classes="flex-1 mar-b-15">
      <Row>
        {propKeys.map((key) => {
          return (
            <PropItem
              key={`${configMapKey}_${key}`}
              propName={key}
              category={category}
              value={data[key]}
              changeHandler={changeHandler}
            />
          )
        })}
      </Row>
    </Panel>
  )
}

export default ConfigItem
