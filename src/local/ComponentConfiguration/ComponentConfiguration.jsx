import React, { PureComponent, cloneElement } from 'react';

import ConfigWrapper from './ConfigWrapper'

class ComponentConfiguration extends PureComponent {
  changeHandler = (data) => {
    const { changeHandler, updateSavedItems, configMapKey } = this.props;
    changeHandler(data);
    updateSavedItems(configMapKey, false);
  };

  render() {
    const { configMapKey, data, children } = this.props
    const childrenWithProps = cloneElement(children, {
      data,
    });
    return (
      <ConfigWrapper
        data={data}
        configMapKey={configMapKey}
        changeHandler={this.changeHandler}
        output={childrenWithProps}
      />
    )
  }
}

export default ComponentConfiguration
