import React, { Fragment, PureComponent } from "react";

import { Button, TextInput, Row, Col, Modal } from '@common';

import MultipleConfigurationModal from './MultipleConfigurationModal.jsx'

class Multiple extends PureComponent {
  state = {
    showModal: false,
  }

  nameChangeHandler = (data) => {
    const { nameChangeHandler, index } = this.props;
    nameChangeHandler(data, index)
  }

  configure = () => {
    this.setState({
      showModal: true
    })
  }

  closeModal = () => {
    this.setState({
      showModal: false,
    })
  }

  submitHandler = (data) => {
    const { submitHandler, index } = this.props
    submitHandler(data, index)
  }

  render() {
    const { showModal } = this.state;
    const { item = {}, configMapKey } = this.props;
    const { name, value } = item;
    return (
      <Fragment>
        <Row classes="align-end">
          <Col col={{ xs: 12, sm: 6, md: 6, lg: 6 }}>
            <TextInput
              name="name"
              value={name}
              label="Name"
              changeHandler={this.nameChangeHandler}
            />
          </Col>
          <Col col={{ xs: 12, sm: 6, md: 6, lg: 6 }}>
            <Button clickHandler={this.configure} disabled={!name}>
              Configure
            </Button>
          </Col>
        </Row>
        <Modal
          show={showModal}
          closeHandler={this.closeModal}
          size="large"
        >
          <MultipleConfigurationModal
            category={name}
            data={value}
            configMapKey={configMapKey}
            closeModal={this.closeModal}
            submitHandler={this.submitHandler}
          />
        </Modal>
      </Fragment>
    );
  }
}

export default Multiple
