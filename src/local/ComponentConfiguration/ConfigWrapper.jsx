import React from 'react'

import { Col, Row } from '@common'

import ConfigItem from './ConfigItem';
import MultipleConfigItem from './MultipleConfigItem.jsx';

const ConfigWrapper = ({ configMapKey, data, output, changeHandler }) => {
  const configKeys = Object.keys(data)
  return (
    <Row>
      <Col col={{ xs: 12, sm: 12, md: 12, lg: 6 }}  classes="mar-b-10 flex flex-column">
        {configKeys.map((key) => {
          const configProps = data[key]
          const isMultipleEntry = (key === 'category' || key === 'size')
          if (isMultipleEntry) {
            return (
              <MultipleConfigItem
                key={`${configMapKey}_${key}`}
                category={key}
                configMapKey={configMapKey}
                changeHandler={changeHandler}
                data={configProps}
              />
            )
          }
          return (
            <ConfigItem
              key={`${configMapKey}_${key}`}
              category={key}
              configMapKey={configMapKey}
              changeHandler={changeHandler}
              data={configProps}
            />
          )
        })}
      </Col>
      <Col col={{ xs: 12, sm: 12, md: 12, lg: 6 }}  classes="mar-b-10 flex flex-column">
        {output}
      </Col>
    </Row>
  )
}

export default ConfigWrapper
