import React, { PureComponent } from 'react';

import { TextInput, Col } from '@common';
import { ConfigSelection } from '@local';

class PropItem extends PureComponent {
  changeHandler = (data) => {
    const { changeHandler, category } = this.props;
    changeHandler({
      [category]: {
        $merge: data
      }
    })
  }

  isSelectionProp = (propName, str) => propName.toLowerCase().indexOf(str) !== -1

  getConfigSelectionKey = (propName) => {
    let configSelectionKey = ''
    if (this.isSelectionProp(propName,'color')) {
      configSelectionKey = 'colors'
    }
    if (this.isSelectionProp(propName, 'typography')) {
      configSelectionKey = 'typography'
    }
    if (this.isSelectionProp(propName, 'spacing')) {
      configSelectionKey = 'spacing'
    }
    if (this.isSelectionProp(propName,'fontweight')) {
      configSelectionKey = 'fontWeight'
    }
    return configSelectionKey
  }

  render() {
    const { propName = '', value } = this.props
    const configSelectionKey = this.getConfigSelectionKey(propName)
    const label = propName.replace(/([A-Z]+)/g, " $1").toLowerCase()
    return (
      <Col col={{ xs: 12, sm: 12, md: 12, lg: 6}} classes="pad-b-10">
        {configSelectionKey && (
          <ConfigSelection
            configKey={configSelectionKey}
            label={label}
            name={propName}
            selected={value}
            changeHandler={this.changeHandler}
          />
        )}
        {!configSelectionKey && (
          <TextInput
            name={propName}
            value={value}
            label={label}
            changeHandler={this.changeHandler}
            attributes={{
              placeholder: ''
            }}
          />
        )}
      </Col>
    )
  }
}

export default PropItem
