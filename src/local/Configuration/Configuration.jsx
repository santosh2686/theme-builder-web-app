import React, { cloneElement, PureComponent } from "react";

import { Row, Col, Panel } from "@common";

import AddRemoveItem from "../AddRemoveItem/AddRemoveItem.jsx";

class Configuration extends PureComponent {
  updateState = (obj) => {
    const { changeHandler } = this.props;
    changeHandler(obj);
  };

  addHandler = () => {
    this.updateState({
      $push: [
        {
          name: "",
          value: "",
        },
      ],
    });
  };

  removeHandler = (index) => {
    this.updateState({
      $splice: [[index, 1]],
    });
  };

  changeHandler = (dataObj) => {
    this.updateState(dataObj);
  };

  render() {
    const { data, children, withOutput } = this.props;

    const configurationChild = cloneElement(withOutput ? children[0] : children, {
      changeHandler: this.changeHandler
    })
    const outputChild = withOutput ? cloneElement(children[1], {
      data,
    }) : null;
    return (
      <Row>
        <Col
          col={{ xs: 12, sm: 12, md: 12, lg: 6 }}
          classes="mar-b-10 flex flex-column"
        >
          <Panel title="Configuration" classes="pad-b-10 flex-1">
            <AddRemoveItem
              addHandler={this.addHandler}
              removeHandler={this.removeHandler}
              itemList={data}
            >
              {configurationChild}
            </AddRemoveItem>
          </Panel>
        </Col>
        {withOutput && (
          <Col
            col={{ xs: 12, sm: 12, md: 12, lg: 6 }}
            classes="mar-b-10 flex flex-column"
          >
            <Panel title="Output" classes="pad-b-10 flex-1">
              {outputChild}
            </Panel>
          </Col>
        )}
      </Row>
    );
  }
}

export default Configuration;
