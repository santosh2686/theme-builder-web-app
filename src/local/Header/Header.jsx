import React, { PureComponent } from 'react'
import { bool, func, string } from 'prop-types';
import { Layout, Text, Icon, Button, Modal } from '@common';

import { ConfigContext } from "@context";

import ExportView from "../ExportView/ExportView.jsx";

class Header extends PureComponent {
  state = {
    showModal: false,
    config: {}
  }

  closeModal = () => {
   this.setState({
     showModal: false
   })
  }

  exportHandler = (config) => {
    this.setState({
      showModal: true,
      config
    })
  }

  render() {
    const { classes, clickHandler } = this.props;
    const { showModal, config } = this.state;
    return(
      <ConfigContext.Consumer>
        {(context) => {
          const { config } = context;
          return (
            <Layout
              flex={{ align: 'center', justify: 'space-between'}}
              bgColor='white'
              pad={{ r: 20, tb: 10 }}
              classes={classes}
            >
              <Layout
                flex={{ align: 'center'}}
              >
                <Text tag="div" color="success" weight="bold" align="center" classes="logo">TB</Text>
                <Text tag="h3" classes="overflow-hidden brand">Theme Builder</Text>
                <Icon name="bars" size="22" classes="cur-pointer" attributes={{
                  onClick: clickHandler
                }} />
              </Layout>
              <div>
                <Button category="success" clickHandler={() => { this.exportHandler(config) }}>
                  Export <Icon name="upload" color="white" classes="mar-l-5" />
                </Button>
              </div>
              <Modal
                show={showModal}
                closeHandler={this.closeModal}
              >
                <ExportView config={config} />
              </Modal>
            </Layout>
          )
        }}

      </ConfigContext.Consumer>
    )
  }
}

export default Header
