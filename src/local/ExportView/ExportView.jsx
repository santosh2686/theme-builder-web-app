import React, { Fragment } from "react";
import { Layout, Button, TextArea, Text, Icon } from "@common";

const ExportView = ({ config, closeModal }) => {
  return(
    <Fragment>
      <Layout flex={{ align: 'center', justify: 'space-between' }} pad={{ tb: 5, lr: 10 }}>
        <h4>
          Theme configuration
        </h4>
        <Button category="clear" clickHandler={closeModal}>
          <Icon name="close" />
        </Button>
      </Layout>
      <Layout pad="10">
        <TextArea
          value={JSON.stringify(config, null, 4)}
          classes="config-text-area"
        />

      </Layout>
    </Fragment>
  )
}

export default ExportView
