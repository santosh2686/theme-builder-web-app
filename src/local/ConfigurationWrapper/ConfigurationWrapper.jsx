import React, { Fragment, cloneElement, PureComponent } from "react";
import { bool, func, node, any, string } from "prop-types";
import update from "immutability-helper"

import { arrayToObject, MapCssModules, objectToArray } from "@utils";

import PageHeader from '../PageHeader/PageHeader.jsx';

class ConfigurationWrapper extends PureComponent {
  state = {
    isLoading: true,
    data: []
  }

  componentDidMount() {
    const { configValue, keepOriginal } = this.props;
    const data = keepOriginal ? configValue : objectToArray(configValue);
    this.setState({ data, isLoading: false });
  }

  changeHandler = (valueObj) => {
    this.setState(prevState => update(prevState, {
      data: valueObj
    }))
  }

  submitHandler = () => {
    const { data } = this.state;
    const { keepOriginal, updateConfig, updateSavedItems, configMapKey, parentMapKey } = this.props;

    const updatedData = keepOriginal ? data : arrayToObject(data);
    let outputObj = {
      $merge: {
        [configMapKey]: updatedData
      }
    }

    if (parentMapKey) {
      outputObj = {
        [parentMapKey]: outputObj
      }
    }

    updateConfig(outputObj)
    updateSavedItems(configMapKey, true)
  };

  render() {
    const { data, isLoading } = this.state;
    if (isLoading) {
      return null
    }
    const { title, children, configMapKey, updateSavedItems } = this.props;
    const childrenWithProps = cloneElement(children, {
      changeHandler: this.changeHandler,
      data,
      configMapKey,
      updateSavedItems,
    });
    return (
      <Fragment>
        <PageHeader
          title={title}
          configMapKey={configMapKey}
          submitHandler={this.submitHandler}
        />
        {childrenWithProps}
      </Fragment>
    );
  }
}

ConfigurationWrapper.propTypes = {
  configValue: any.isRequired,
  title: string,
  configMapKey: string,
  keepOriginal: bool,
  updateConfig: func.isRequired,
  updateSavedItems: func.isRequired,
  children: node.isRequired
}

ConfigurationWrapper.defaultProps = {
  title: 'Configuration',
  configMapKey: '',
  keepOriginal: false,
}

export default MapCssModules(ConfigurationWrapper)
