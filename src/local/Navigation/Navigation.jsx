import React, { PureComponent } from 'react'
import { withRouter } from 'react-router-dom';
import { bool, func, string } from 'prop-types';
import ClassNames from 'classnames';
import { MapCssModules } from '@utils'
import { routeConfig } from '@config'

import MenuItem from './MenuItem.jsx'
import SubMenuItem from './SubMenuItem.jsx';

class Navigation extends PureComponent {
  state = {
    expandedIndex: -1
  };

  clickHandler = (index) => {
    this.setState((prevState) => {
      const { expandedIndex } = prevState;
      return {
        expandedIndex: expandedIndex === index ? -1 : index
      }
    })
  };

  render() {
    const { location: { pathname }, classes } = this.props;
    const { expandedIndex } = this.state;
    const navClasses = ClassNames('overflow-auto bg-theme side-bar', {
      [classes]: classes
    });
    return (
      <aside styleName={navClasses}>
        <ul>
          {routeConfig.map((route, index) => {
            if(route.items) {
              return (
                <SubMenuItem
                  clickHandler={this.clickHandler}
                  index={index}
                  expand={expandedIndex === index}
                  pathname={pathname}
                  key={route.label}
                  item={route} />
              )
            }
            return (
              <MenuItem pathname={pathname} key={route.label} item={route} />
            )
          })}
        </ul>
      </aside>
    )
  }
}

export default withRouter(MapCssModules(Navigation))
