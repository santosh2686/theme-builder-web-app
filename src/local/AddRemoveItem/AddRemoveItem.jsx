import React, { cloneElement, Fragment } from "react";
import { func, node, arrayOf, shape } from "prop-types";
import { Layout, LayoutItem, Button, Icon } from "@common";

const AddRemoveItem = ({ children, itemList = [], removeHandler, addHandler }) => (
  <Fragment>
    {itemList.map((item, index) => {
      const childrenWithProps = cloneElement(children, { item, index });
      return (
        <Layout
          key={index}
          mar={{ t: 15 }}
          bgColor="gray-lighter"
          border="gray-light"
          pad="10"
          flex={{ justify: "space-between", align: "end" }}
        >
          <LayoutItem full>
            {childrenWithProps}
          </LayoutItem>
          <LayoutItem classes="pad-l-15">
            <Button
              disabled={itemList.length === 1}
              category="danger"
              clickHandler={() => removeHandler(index)}
            >
              <Icon name="close" color="white" />
            </Button>
          </LayoutItem>
        </Layout>
      );
    })}
    <Layout pad={{ t: 15, r: 10 }} classes="text-right">
      <Button clickHandler={addHandler}>
        <Icon name="plus" color="white" />
      </Button>
    </Layout>
  </Fragment>
);

AddRemoveItem.propTypes = {
  children: node.isRequired,
  itemList: arrayOf(
    shape({
      name: String,
      value: String,
    })
  ),
  removeHandler: func.isRequired,
  addHandler: func.isRequired,
};

export default AddRemoveItem;
