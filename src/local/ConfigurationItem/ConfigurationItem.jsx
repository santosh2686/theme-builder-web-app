import React, { PureComponent } from "react";
import { string, func, oneOfType, number, shape } from "prop-types"
import { Row, Col, TextInput } from "@common";

class ConfigurationItem extends PureComponent {
  changeHandler = (data) => {
    const { changeHandler, index, updateSavedItems, configMapKey } = this.props;
    changeHandler({
      [index]: {
        $merge: data,
      },
    });
    updateSavedItems(configMapKey, false)
  };

  render() {
    const { item, namePlaceholder, valuePlaceholder } = this.props;
    const { name, value } = item;
    return (
      <Row>
        <Col col={{ xs: 12, sm: 12, md: 12, lg: 6 }}>
          <TextInput
            name="name"
            label="Name"
            value={name}
            changeHandler={this.changeHandler}
            attributes={{
              placeholder: `e.g. ${namePlaceholder}`,
            }}
          />
        </Col>
        <Col col={{ xs: 12, sm: 12, md: 12, lg: 6 }}>
          <TextInput
            name="value"
            label="Value"
            value={value}
            changeHandler={this.changeHandler}
            attributes={{
              placeholder: `e.g. ${valuePlaceholder}`,
            }}
          />
        </Col>
      </Row>
    );
  }
}

ConfigurationItem.propTypes = {
  index: number,
  changeHandler: func,
  updateSavedItems: func.isRequired,
  item: shape({
    name: oneOfType([string, number]),
    value: oneOfType([string, number])
  }),
  configMapKey: string.isRequired,
  namePlaceholder: string,
  valuePlaceholder: string.isRequired
}

ConfigurationItem.defaultProps = {
  index: 0,
  item: {
    name: '',
    value: ''
  },
  namePlaceholder: 'primary',
  changeHandler: () => {}
}

export default ConfigurationItem;
