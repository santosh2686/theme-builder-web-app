import React, { memo } from 'react';

import { ConfigContext } from "@context";
import { Layout, Button, Icon } from "@common";

const PageHeader = ({ title, submitHandler, configMapKey }) => {
  return (
    <Layout
      flex={{ align: 'center', justify: 'space-between' }}
      pad={{ b: 5 }}
      mar={{ b: 15 }}
    >
      <h4>{title}</h4>
      <ConfigContext.Consumer>
        {context => {
          const { savedItems } = context;
          const isSaved = savedItems[configMapKey]
          return (
            <Button clickHandler={submitHandler} disabled={isSaved}>
              { isSaved ? 'Saved' : 'Save' }
              {isSaved && (<Icon name="check" color="white" classes="mar-l-10"/>)}
            </Button>
          )
        }}
      </ConfigContext.Consumer>
    </Layout>
  )
}

export default memo(PageHeader)
