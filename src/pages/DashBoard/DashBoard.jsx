import React from "react";
import { Layout, Row, Col, Text, Icon } from "@common";

const DashBoard = ({ onSelection }) => {
  return (
   <Layout flex={{ align: "center", justify: "center", direction: "column" }} classes="height-100">
       <Layout classes="dashboard">
           <Text tag="h2" classes="text-center pad-b-30">Theme Builder</Text>
           <Row>
               <Col col={{ xs: 12, sm: 12, md: 6, lg: 6 }} classes="mar-b-15">
                   <Layout
                     bgColor="white"
                     flex={{ align: "center", justify: "center", direction: "column" }}
                     classes="dash-item"
                     pad={{ tb: 30 }}
                     attributes={{
                         onClick: onSelection
                     }}
                   >
                       <Text tag="h4" classes="pad-b-30">Create from base</Text>
                       <Layout classes="dash-icon pad-l-10 pad-t-10 pad-r-10 pad-b-5">
                           <Icon name="pencil-square-o" color="danger" iconScale="5x" />
                       </Layout>
                   </Layout>
               </Col>
               <Col col={{ xs: 12, sm: 12, md: 6, lg: 6 }} classes="mar-b-15">
                   <Layout
                     bgColor="white"
                     flex={{ align: "center", justify: "center", direction: "column" }}
                     classes="dash-item"
                     pad={{ tb: 30 }}
                     attributes={{
                         onClick: onSelection
                     }}
                   >
                       <Text tag="h4" classes="pad-b-30">Import existing</Text>
                       <Layout classes="dash-icon pad-l-10 pad-t-10 pad-r-10 pad-b-5">
                           <Icon name="cubes" color="danger" iconScale="5x" />
                       </Layout>
                   </Layout>
               </Col>
           </Row>
       </Layout>
   </Layout>
  )
}

export default DashBoard
