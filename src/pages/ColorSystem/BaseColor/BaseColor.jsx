import React, { Fragment } from "react";

import { ConfigContext } from "@context";

import { ConfigurationWrapper, Configuration, ConfigurationItem } from "@local";

import ColorOutput from "../ColorOutput/ColorOutput.jsx";

const BaseColor = () => {
  return (
    <Fragment>
      <ConfigContext.Consumer>
        {(context) => {
          const { config, updateConfig, updateSavedItems } = context;
          const { baseColors } = config;
          return (
            <ConfigurationWrapper
              configValue={baseColors}
              title="Base Colors"
              configMapKey="baseColors"
              updateConfig={updateConfig}
              updateSavedItems={updateSavedItems}
            >
              <Configuration withOutput>
                <ConfigurationItem
                   namePlaceholder="primary"
                   valuePlaceholder="#FFFFFF"
                   configMapKey="baseColors"
                   updateSavedItems={updateSavedItems}
                />
                <ColorOutput />
              </Configuration>
            </ConfigurationWrapper>
          );
        }}
      </ConfigContext.Consumer>
    </Fragment>
  );
};

export default BaseColor;
