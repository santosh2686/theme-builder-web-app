import React, { Fragment } from "react";
import { ConfigContext } from "@context";

import { ConfigurationWrapper, Configuration, ConfigurationItem } from "@local";

import ColorOutput from "../ColorOutput/ColorOutput.jsx";

const TransparentColor = () => {
  return (
    <Fragment>
      <ConfigContext.Consumer>
        {(context) => {
          const { config, updateConfig, updateSavedItems } = context;
          const { transparentBackground } = config;
          return (
            <ConfigurationWrapper
              configValue={transparentBackground}
              title="Transparent background colors"
              configMapKey="transparentBackground"
              updateConfig={updateConfig}
              updateSavedItems={updateSavedItems}
            >
              <Configuration withOutput>
                <ConfigurationItem
                   namePlaceholder="low"
                   valuePlaceholder="rgba(0, 0, 0, 0.5)"
                   configMapKey="transparentBackground"
                   updateSavedItems={updateSavedItems}
                />
                <ColorOutput />
              </Configuration>
            </ConfigurationWrapper>
          );
        }}
      </ConfigContext.Consumer>
    </Fragment>
  );
};

export default TransparentColor;
