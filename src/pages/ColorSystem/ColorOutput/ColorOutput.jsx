import React from "react";
import { Layout } from "@common";
import { MapCssModules } from '@utils';

const ColorOutput = ({ data }) => {
  return (
    <Layout flex={{ wrap: true }}>
      {data.map(({ name, value }) => (
        <Layout pad="15" key={name} classes="text-center">
          <div styleName="sample-color-box mar-b-5" style={{
            backgroundColor: value
          }} />
          <span>{name}</span>
        </Layout>
      ))}
    </Layout>
  );
};

export default MapCssModules(ColorOutput);
