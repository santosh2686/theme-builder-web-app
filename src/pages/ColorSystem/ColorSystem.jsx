import React from "react";
import { shape, string } from "prop-types";
import { Route, Switch, Redirect } from "react-router-dom";

import BaseColor from "./BaseColor/BaseColor.jsx";
import BrandColor from "./BrandColor/BrandColor.jsx";
import Transparent from "./Transparent/Transparent.jsx";

const ColorSystem = ({
    match: { url }
}) => {
  return (
    <Switch>
      <Route exact path={url} render={() => <Redirect to={`${url}/base-color`} />} />
      <Route path={`${url}/base-color`} component={BaseColor} />
      <Route path={`${url}/brand-color`} component={BrandColor} />
      <Route path={`${url}/transparent`} component={Transparent} />
      <Route render={() => <Redirect to={`${url}/base-color`} />} />
    </Switch>
  );
};

ColorSystem.prototypes = {
    match: shape({
        url: string
    })
};

ColorSystem.defaultProps = {
    match: {
        url: '',
    }
};

export default ColorSystem;
