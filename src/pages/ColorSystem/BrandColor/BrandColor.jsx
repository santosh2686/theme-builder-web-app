import React, { Fragment } from "react";
import { ConfigContext } from "@context";

import { ConfigurationWrapper, Configuration, ConfigurationItem } from "@local";

import ColorOutput from "../ColorOutput/ColorOutput.jsx";

const BrandColor = () => {
  return (
    <Fragment>
      <ConfigContext.Consumer>
        {(context) => {
          const { config, updateConfig, updateSavedItems } = context;
          const { brandColors } = config;
          return (
            <ConfigurationWrapper
              configValue={brandColors}
              title="Brand Colors"
              configMapKey="brandColors"
              updateConfig={updateConfig}
              updateSavedItems={updateSavedItems}
            >
              <Configuration withOutput>
                <ConfigurationItem
                   namePlaceholder="primary"
                   valuePlaceholder="#FFFFFF"
                   configMapKey="brandColors"
                   updateSavedItems={updateSavedItems}
                />
                <ColorOutput />
              </Configuration>
            </ConfigurationWrapper>
          );
        }}
      </ConfigContext.Consumer>
    </Fragment>
  );
};

export default BrandColor;
