import React, { Fragment } from "react";

import { ConfigContext } from "@context";

import { ConfigurationWrapper } from "@local";

import PageBodyConfiguration from "./PageBodyConfiguration/PageBodyConfiguration.jsx";

const PageBody = () => {
  return (
    <Fragment>
      <ConfigContext.Consumer>
        {(context) => {
          const { config, updateConfig, updateSavedItems } = context;
          const { pageBody } = config;
          return (
            <ConfigurationWrapper
              configValue={pageBody}
              title="Page body"
              configMapKey="pageBody"
              updateConfig={updateConfig}
              updateSavedItems={updateSavedItems}
              keepOriginal
            >
              <PageBodyConfiguration
                configMapKey="pageBody"
                updateSavedItems={updateSavedItems}
              />
            </ConfigurationWrapper>
          );
        }}
      </ConfigContext.Consumer>
    </Fragment>
  );
};

export default PageBody;
