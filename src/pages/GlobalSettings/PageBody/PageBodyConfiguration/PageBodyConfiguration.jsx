import React, { PureComponent } from "react";

import { MapCssModules } from '@utils';
import { Panel, Row, Col } from "@common";
import { ConfigSelection } from "@local";
import { ConfigContext } from "@context";

class PageBodyConfiguration extends PureComponent {
  changeHandler = (data) => {
    const { changeHandler, updateSavedItems, configMapKey } = this.props;
    changeHandler({
      $merge: data
    });
    updateSavedItems(configMapKey, false);
  };

  render() {
    const { data } = this.props;
    const { backgroundColor } = data;
    return (
      <Row>
        <Col col={{ xs: 12, sm: 12, md: 12, lg: 6 }}  classes="mar-b-10 flex flex-column">
          <Panel title="Configuration" classes="flex-1">
            <Row>
              <Col col={{ xs: 12, sm: 12, md: 6, lg: 4 }} classes="pad-b-10">
                <ConfigSelection
                  configKey="colors"
                  label="Page body background"
                  name="backgroundColor"
                  selected={backgroundColor}
                  changeHandler={this.changeHandler}
                />
              </Col>
            </Row>
          </Panel>
        </Col>
        <Col col={{ xs: 12, sm: 12, md: 12, lg: 6 }} classes="mar-b-10 flex flex-column">
          <Panel title="Output" classes="pad-b-15 flex-1">
            <ConfigContext.Consumer>
              {(context) => {
                const { getConfigValue } = context;
                console.log(getConfigValue('colors', backgroundColor))
                return (
                  <div className="sample-color-box mar-b-5" style={{
                    backgroundColor: getConfigValue('colors', backgroundColor)
                  }} />
                )
              }}
            </ConfigContext.Consumer>
          </Panel>
        </Col>
      </Row>
    );
  }
}

export default MapCssModules(PageBodyConfiguration);
