import React, { Fragment } from "react";

import { ConfigContext } from "@context";

import { ConfigurationWrapper } from "@local";

import TextSelectionConfiguration from "./TextSelectionConfiguration/TextSelectionConfiguration.jsx";

const TextSelection = () => {
  return (
    <Fragment>
      <ConfigContext.Consumer>
        {(context) => {
          const { config, updateConfig, updateSavedItems } = context;
          const { textSelection, baseColors, brandColors } = config;
          return (
            <ConfigurationWrapper
              configValue={textSelection}
              title="Text selection"
              configMapKey="textSelection"
              updateConfig={updateConfig}
              updateSavedItems={updateSavedItems}
              keepOriginal
            >
              <TextSelectionConfiguration
                configMapKey="textSelection"
                baseColors={baseColors}
                brandColors={brandColors}
                updateSavedItems={updateSavedItems}
              />
            </ConfigurationWrapper>
          );
        }}
      </ConfigContext.Consumer>
    </Fragment>
  );
};

export default TextSelection;
