import React, { PureComponent } from "react";

import { MapCssModules } from '@utils';
import { Panel, Row, Col, SelectBox } from "@common";

class TextSelectionConfiguration extends PureComponent {
  changeHandler = (data) => {
    const { changeHandler, updateSavedItems, configMapKey } = this.props;
    changeHandler({
      $merge: data
    });
    updateSavedItems(configMapKey, false);
  };

  computeOptions = (objectInput) => {
    return Object.keys(objectInput).reduce((acc, next) => {
      acc.push({
        key: next,
        value: next
      });
      return acc;
    },[])
  }

  render() {
    const { data, baseColors, brandColors } = this.props;
    const colorOptions = {
      ...baseColors,
      ...brandColors,
    }
    const colorsOptions = this.computeOptions(colorOptions)

    const selectedBgColor = colorOptions[data.backgroundColor]
    const selectedTextColor = colorOptions[data.textColor]

    return (
      <Row>
        <Col col={{ xs: 12, sm: 12, md: 12, lg: 6 }}  classes="mar-b-10 flex flex-column">
          <Panel title="Configuration" classes="flex-1">
            <Row>
              <Col col={{ xs: 12, sm: 12, md: 6, lg: 6 }} classes="pad-b-10">
                <SelectBox
                  label="Background color"
                  name="backgroundColor"
                  value={data.backgroundColor}
                  options={colorsOptions}
                  changeHandler={this.changeHandler}
                />
              </Col>
              <Col col={{ xs: 12, sm: 12, md: 6, lg: 6 }} classes="pad-b-10">
                <SelectBox
                  label="Text color"
                  name="textColor"
                  value={data.textColor}
                  options={colorsOptions}
                  changeHandler={this.changeHandler}
                />
              </Col>
            </Row>
          </Panel>
        </Col>
        <Col col={{ xs: 12, sm: 12, md: 12, lg: 6 }} classes="mar-b-10 flex flex-column">
          <Panel title="Output" classes="pad-b-15 flex-1">
            <div styleName="pad-b-20">
              <span styleName="pad-tb-5 pad-lr-10" style={{
                backgroundColor: selectedBgColor,
                color: selectedTextColor
              }}>
                The quick brown for jumps over the lazy dog.
              </span>
            </div>
          </Panel>
        </Col>
      </Row>
    );
  }
}

export default MapCssModules(TextSelectionConfiguration);
