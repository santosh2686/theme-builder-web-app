import React from "react";
import { shape, string } from "prop-types";
import { Route, Switch, Redirect } from "react-router-dom";

import PageBody from './PageBody/PageBody.jsx';
import DefaultTypography from './DefaultTypography/DefaultTypography.jsx';
import Headings from './Headings/Headings.jsx';
import TextSelection from './TextSelection/TextSelection.jsx';

const GlobalSettings = ({
  match: { url }
}) => {
  return (
    <Switch>
      <Route exact path={url} render={() => <Redirect to={`${url}/page-body`} />} />
      <Route path={`${url}/page-body`} component={PageBody} />
      <Route path={`${url}/default-typography`} component={DefaultTypography} />
      <Route path={`${url}/headings`} component={Headings} />
      <Route path={`${url}/text-selection`} component={TextSelection} />
      <Route render={() => <Redirect to={`${url}/page-body`} />} />
    </Switch>
  );
};

GlobalSettings.prototypes = {
  match: shape({
    url: string
  })
};

GlobalSettings.defaultProps = {
  match: {
    url: '',
  }
};

export default GlobalSettings
