import React, { Fragment } from "react";

import { ConfigContext } from "@context";

import { ConfigurationWrapper } from "@local";

import DefaultTypographyConfiguration from "./DefaultTypographyConfiguration/DefaultTypographyConfiguration.jsx";

const PageBody = () => {
  return (
    <Fragment>
      <ConfigContext.Consumer>
        {(context) => {
          const { config, updateConfig, updateSavedItems } = context;
          const { defaultTypography, typography, fontWeight } = config;
          return (
            <ConfigurationWrapper
              configValue={defaultTypography}
              title="Default typography"
              configMapKey="defaultTypography"
              updateConfig={updateConfig}
              updateSavedItems={updateSavedItems}
              keepOriginal
            >
              <DefaultTypographyConfiguration
                configMapKey="defaultTypography"
                typography={typography}
                fontWeight={fontWeight}
                updateSavedItems={updateSavedItems}
              />
            </ConfigurationWrapper>
          );
        }}
      </ConfigContext.Consumer>
    </Fragment>
  );
};

export default PageBody;
