import React, { PureComponent } from "react";

import { MapCssModules } from '@utils';
import { Panel, Row, Col, TextInput } from "@common";
import { ConfigSelection } from "@local";

class DefaultTypographyConfiguration extends PureComponent {
  changeHandler = (data) => {
    const { changeHandler, updateSavedItems, configMapKey } = this.props;
    changeHandler({
      $merge: data
    });
    updateSavedItems(configMapKey, false);
  };

  render() {
    const { data, typography, fontWeight } = this.props;
    const selectedTypography = typography[data.typography] || {}
    const selectedFontWeight = fontWeight[data.fontWeight] || {}
    const { fontSize, letterSpacing, lineHeight } = selectedTypography

    return (
      <Row>
        <Col col={{ xs: 12, sm: 12, md: 12, lg: 6 }}  classes="mar-b-10 flex flex-column">
          <Panel title="Configuration" classes="flex-1">
            <Row>
              <Col col={{ xs: 12, sm: 12, md: 6, lg: 6 }} classes="pad-b-10">
                <TextInput
                    label="Font family"
                    name="fontFamily"
                    value={data.fontFamily}
                    changeHandler={this.changeHandler}
                />
              </Col>
              <Col col={{ xs: 12, sm: 12, md: 6, lg: 6 }} classes="pad-b-10">
                <ConfigSelection
                  configKey="typography"
                  label="Default typography"
                  name="typography"
                  selected={data.typography}
                  changeHandler={this.changeHandler}
                />
              </Col>
            </Row>
            <Row>
              <Col col={{ xs: 12, sm: 12, md: 6, lg: 6 }} classes="pad-b-10">
                <ConfigSelection
                  configKey="fontWeight"
                  label="Default font weight"
                  name="fontWeight"
                  selected={data.fontWeight}
                  changeHandler={this.changeHandler}
                />
              </Col>
            </Row>
          </Panel>
        </Col>
        <Col col={{ xs: 12, sm: 12, md: 12, lg: 6 }} classes="mar-b-10 flex flex-column">
          <Panel title="Output" classes="pad-b-15 flex-1">
            <div styleName="pad-b-20">
              <div
                style={{
                  fontFamily: data.fontFamily,
                  fontSize,
                  letterSpacing,
                  lineHeight,
                  fontWeight: selectedFontWeight,
                }}
              >
                E.g.: The quick brown for jumps over the lazy dog.
              </div>
              <div styleName="color-gray pad-t-5">
               font-size: {fontSize} &nbsp; &nbsp;
                letter-spacing: {letterSpacing} &nbsp; &nbsp; line-height:{" "}
                {lineHeight}
              </div>
            </div>
          </Panel>
        </Col>
      </Row>
    );
  }
}

export default MapCssModules(DefaultTypographyConfiguration);
