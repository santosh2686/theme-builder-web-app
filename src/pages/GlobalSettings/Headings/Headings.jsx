import React from "react";

import { ConfigContext } from "@context";

import { ConfigurationWrapper } from "@local";

import HeadingConfiguration from "./HeadingConfiguration/HeadingConfiguration.jsx";

const Headings = () => (
  <ConfigContext.Consumer>
    {(context) => {
      const { config, updateConfig, updateSavedItems } = context;
      const { headings, typography } = config;
      return (
        <ConfigurationWrapper
          configValue={headings}
          title="Headings"
          configMapKey="headings"
          updateConfig={updateConfig}
          updateSavedItems={updateSavedItems}
          keepOriginal
        >
          <HeadingConfiguration
            configMapKey="headings"
            typography={typography}
            updateSavedItems={updateSavedItems}
          />
        </ConfigurationWrapper>
      );
    }}
  </ConfigContext.Consumer>
);

export default Headings;
