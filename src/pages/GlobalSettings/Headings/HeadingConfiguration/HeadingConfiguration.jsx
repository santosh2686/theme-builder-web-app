import React, { PureComponent } from "react";

import { MapCssModules } from '@utils';
import { Layout, Panel, Row, Col, SelectBox, Text } from "@common";

class HeadingConfiguration extends PureComponent {
  changeHandler = (data) => {
    const { changeHandler, updateSavedItems, configMapKey } = this.props;
    changeHandler({
      $merge: data
    });
    updateSavedItems(configMapKey, false);
  };

  computeOptions = (objectInput) => {
    return Object.keys(objectInput).reduce((acc, next) => {
      acc.push({
        key: next,
        value: next
      });
      return acc;
    },[])
  }

  render() {
    const { data, typography } = this.props;
    const options = this.computeOptions(typography);
    return (
      <Row>
        <Col col={{ xs: 12, sm: 12, md: 12, lg: 6 }}  classes="mar-b-10 flex flex-column">
          <Panel title="Configuration" classes="flex-1">
            {
              Object.keys(data).map((key) => {
                return (
                  <Row key={key}>
                    <Col col={{ xs: 12, sm: 12, md: 12, lg: 3 }}  classes="mar-b-15">
                        <Text tag="label">&nbsp;</Text>
                        <Text tag="div" color="gray" align="center">{key}</Text>
                    </Col>
                    <Col col={{ xs: 12, sm: 12, md: 12, lg: 9 }}  classes="mar-b-15">
                      <SelectBox
                        label="Select typography"
                        name={key}
                        value={data[key]}
                        options={options}
                        changeHandler={this.changeHandler}
                      />
                    </Col>
                  </Row>
                )
              })
            }
          </Panel>
        </Col>
        <Col col={{ xs: 12, sm: 12, md: 12, lg: 6 }} classes="mar-b-10 flex flex-column">
          <Panel title="Output" classes="pad-b-15 flex-1">
            {
              Object.keys(data).map((key, index) => {
                const typographyName = data[key]
                const { fontSize, letterSpacing, lineHeight } = typography[typographyName]
                return(
                  <div styleName="pad-tb-15" key={index}>
                    <Layout bgColor="gray-lighter" mar={{ b: 5 }} pad="5">{key}</Layout>
                    <div
                      style={{
                        fontSize,
                        letterSpacing,
                        lineHeight
                      }}
                    >
                      The quick brown for jumps over the lazy dog.
                    </div>
                  </div>
                )
              })
            }
          </Panel>
        </Col>
      </Row>
    );
  }
}

export default MapCssModules(HeadingConfiguration);
