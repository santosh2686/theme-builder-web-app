import React from 'react';
import { ConfigContext } from "@context/index"
import { ComponentConfiguration, ConfigurationWrapper } from "@local/index"

import BadgeOutput from './BadgeOutput/BadgeOutput.jsx';

const Badge = () => (
  <ConfigContext.Consumer>
      { (context) => {
          const { config, updateConfig, updateSavedItems } = context;
          const { components: { badge } } = config;
          return (
            <ConfigurationWrapper
              configValue={badge}
              title="Badge"
              configMapKey="badge"
              parentMapKey="components"
              updateConfig={updateConfig}
              updateSavedItems={updateSavedItems}
              keepOriginal
            >
                <ComponentConfiguration>
                  <BadgeOutput />
                </ComponentConfiguration>
            </ConfigurationWrapper>
          );
      } }
  </ConfigContext.Consumer>
)

export default Badge
