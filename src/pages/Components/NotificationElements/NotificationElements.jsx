import React from "react";
import { shape, string } from "prop-types";
import { Route, Switch, Redirect } from "react-router-dom";

import Alert from './Alert/Alert.jsx';
import Badge from './Badge/Badge.jsx';
import Modal from './Modal/Modal.jsx';
import ProgressBar from './ProgressBar/ProgressBar.jsx';

const NotificationElements = ({
  match: { url }
}) => {
  return (
    <Switch>
      <Route exact path={url} render={() => <Redirect to={`${url}/alert`} />} />
      <Route exact path={`${url}/alert`} component={Alert} />
      <Route exact path={`${url}/badge`} component={Badge} />
      <Route exact path={`${url}/modal`} component={Modal} />
      <Route exact path={`${url}/progress-bar`} component={ProgressBar} />
      <Route render={() => <Redirect to={`${url}/alert`} />} />
    </Switch>
  );
};

NotificationElements.prototypes = {
  match: shape({
    url: string
  })
};

NotificationElements.defaultProps = {
  match: {
    url: '',
  }
};

export default NotificationElements
