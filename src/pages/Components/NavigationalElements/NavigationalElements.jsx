import React from "react";
import { shape, string } from "prop-types";
import { Route, Switch, Redirect } from "react-router-dom";

import BreadCrumb from './BreadCrumb/BreadCrumb.jsx';
import Anchor from './Anchor/Anchor.jsx';
import ListView from './ListView/ListView.jsx';
import Pagination from './Pagination/Pagination.jsx';
import Tabs from './Tabs/Tabs.jsx';
import Stepper from './Stepper/Stepper.jsx';

const NavigationalElements = ({
  match: { url }
}) => {
  return (
    <Switch>
      <Route exact path={url} render={() => <Redirect to={`${url}/bread-crumb`} />} />
      <Route exact path={`${url}/bread-crumb`} component={BreadCrumb} />
      <Route exact path={`${url}/anchor`} component={Anchor} />
      <Route exact path={`${url}/list-view`} component={ListView} />
      <Route exact path={`${url}/pagination`} component={Pagination} />
      <Route exact path={`${url}/tabs`} component={Tabs} />
      <Route exact path={`${url}/stepper`} component={Stepper} />
      <Route render={() => <Redirect to={`${url}/bread-crumb`} />} />
    </Switch>
  );
};

NavigationalElements.prototypes = {
  match: shape({
    url: string
  })
};

NavigationalElements.defaultProps = {
  match: {
    url: '',
  }
};

export default NavigationalElements
