import React from "react";
import { shape, string } from "prop-types";
import { Route, Switch, Redirect } from "react-router-dom";

import Container from './Container/Container.jsx';
import Grid from './Grid/Grid.jsx';
import Table from './Table/Table.jsx';

const LayoutElements = ({
  match: { url }
}) => {
  return (
    <Switch>
      <Route exact path={url} render={() => <Redirect to={`${url}/container`} />} />
      <Route exact path={`${url}/container`} component={Container} />
      <Route exact path={`${url}/grid`} component={Grid} />
      <Route exact path={`${url}/table`} component={Table} />
      <Route render={() => <Redirect to={`${url}/container`} />} />
    </Switch>
  );
};

LayoutElements.prototypes = {
  match: shape({
    url: string
  })
};

LayoutElements.defaultProps = {
  match: {
    url: '',
  }
};

export default LayoutElements
