import React from 'react';
import { shape, string } from "prop-types";
import { Route, Switch, Redirect } from "react-router-dom";

import Accordion from './Accordion/Accordion.jsx';
import Chip from './Chip/Chip.jsx';
import Collapsible from './Collapsible/Collapsible.jsx';
import PopOver from './PopOver/PopOver.jsx';
import Spinner from './Spinner/Spinner.jsx';
import Tooltip from './Tooltip/Tooltip.jsx';

const PresentationalElements = ({
  match: { url }
}) => {
  return (
    <Switch>
      <Route exact path={url} render={() => <Redirect to={`${url}/accordion`} />} />
      <Route exact path={`${url}/accordion`} component={Accordion} />
      <Route exact path={`${url}/chip`} component={Chip} />
      <Route exact path={`${url}/collapsible`} component={Collapsible} />
      <Route exact path={`${url}/pop-over`} component={PopOver} />
      <Route exact path={`${url}/spinner`} component={Spinner} />
      <Route exact path={`${url}/tooltip`} component={Tooltip} />
      <Route render={() => <Redirect to={`${url}/accordion`} />} />
    </Switch>
  );
};

PresentationalElements.prototypes = {
  match: shape({
    url: string
  })
};

PresentationalElements.defaultProps = {
  match: {
    url: '',
  }
};

export default PresentationalElements
