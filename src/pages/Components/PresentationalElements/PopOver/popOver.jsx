import React from 'react';

import { ConfigContext } from '@context'
import { ComponentConfiguration, ConfigurationWrapper } from '@local'

import PopOverOutput from './PopOverOutput/PopOverOutput.jsx'

const Tooltip = () => (
  <ConfigContext.Consumer>
      { (context) => {
          const { config, updateConfig, updateSavedItems } = context;
          const { components: { popOver } } = config;
          return (
            <ConfigurationWrapper
              configValue={popOver}
              title="Pop Over"
              configMapKey="popOver"
              parentMapKey="components"
              updateConfig={updateConfig}
              updateSavedItems={updateSavedItems}
              keepOriginal
            >
                <ComponentConfiguration>
                  <PopOverOutput />
                </ComponentConfiguration>
            </ConfigurationWrapper>
          );
      } }
  </ConfigContext.Consumer>
)

export default Tooltip
