import React, { Fragment } from 'react'

import { ConfigContext } from "@context";
import { Panel, Row, Col } from '@common'

const SpinnerOutput = ({ data }) => (
  <ConfigContext.Consumer>
    {(context) => {
      const { getConfigValue } = context;
      
      return (
        <Fragment>
          <Panel title="Label" classes="mar-b-15 pad-b-15">
            
          </Panel>
          <Panel title="Output">
            <Row>
              <Col col={{ xs: 3 }} classes="pad-b-15">
                
              </Col>
              <Col col={{ xs: 3 }} classes="pad-b-15">
                
              </Col>
              <Col col={{ xs: 3 }} classes="pad-b-15">
                
              </Col>
              <Col col={{ xs: 3 }} classes="pad-b-15">
                
              </Col>
            </Row>
          </Panel>
        </Fragment>
      )
    }}
  </ConfigContext.Consumer>
)

export default SpinnerOutput
