import React from 'react';

import { ConfigContext } from '@context'
import { ComponentConfiguration, ConfigurationWrapper } from '@local'

import SpinnerOutput from './SpinnerOutput/SpinnerOutput.jsx'

const Spinner = () => (
  <ConfigContext.Consumer>
      { (context) => {
          const { config, updateConfig, updateSavedItems } = context;
          const { components: { spinner } } = config;
          return (
            <ConfigurationWrapper
              configValue={spinner}
              title="Spinner"
              configMapKey="spinner"
              parentMapKey="components"
              updateConfig={updateConfig}
              updateSavedItems={updateSavedItems}
              keepOriginal
            >
                <ComponentConfiguration>
                  <SpinnerOutput />
                </ComponentConfiguration>
            </ConfigurationWrapper>
          );
      } }
  </ConfigContext.Consumer>
)

export default Spinner
