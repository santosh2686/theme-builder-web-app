import React from 'react';

import { ConfigContext } from '@context'
import { ComponentConfiguration, ConfigurationWrapper } from '@local'

import CollapsibleOutput from './CollapsibleOutput/CollapsibleOutput.jsx'

const Collapsible = () => (
  <ConfigContext.Consumer>
      { (context) => {
          const { config, updateConfig, updateSavedItems } = context;
          const { components: { collapsible } } = config;
          return (
            <ConfigurationWrapper
              configValue={collapsible}
              title="Collapsible"
              configMapKey="collapsible"
              parentMapKey="components"
              updateConfig={updateConfig}
              updateSavedItems={updateSavedItems}
              keepOriginal
            >
                <ComponentConfiguration>
                  <CollapsibleOutput />
                </ComponentConfiguration>
            </ConfigurationWrapper>
          );
      } }
  </ConfigContext.Consumer>
)

export default Collapsible
