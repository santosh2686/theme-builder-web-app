import React from 'react';

import { ConfigContext } from '@context'
import { ComponentConfiguration, ConfigurationWrapper } from '@local'

import TooltipOutput from './TooltipOutput/TooltipOutput.jsx'

const Tooltip = () => (
  <ConfigContext.Consumer>
      { (context) => {
          const { config, updateConfig, updateSavedItems } = context;
          const { components: { tooltip } } = config;
          return (
            <ConfigurationWrapper
              configValue={tooltip}
              title="Tooltip"
              configMapKey="tooltip"
              parentMapKey="components"
              updateConfig={updateConfig}
              updateSavedItems={updateSavedItems}
              keepOriginal
            >
                <ComponentConfiguration>
                  <TooltipOutput />
                </ComponentConfiguration>
            </ConfigurationWrapper>
          );
      } }
  </ConfigContext.Consumer>
)

export default Tooltip
