import React from 'react';
import { shape, string } from 'prop-types';
import { Route, Switch, Redirect } from 'react-router-dom';

import LayoutElements from './LayoutElements/LayoutElements.jsx'
import FormElements from './FormElements/FormElements.jsx'
import NavigationalElements from './NavigationalElements/NavigationalElements.jsx'
import NotificationElements from './NotificationElements/NotificationElements.jsx'
import PresentationalElements from './PresentationalElements/PresentationalElements.jsx'

const Components = ({
    match: { url }
}) => {
  return (
    <Switch>
      <Route exact path={url} render={() => <Redirect to={`${url}/layout-components`} />} />
      <Route path={`${url}/layout-components`} component={LayoutElements} />
      <Route path={`${url}/form-components`} component={FormElements} />
      <Route path={`${url}/navigational-components`} component={NavigationalElements} />
      <Route path={`${url}/notification-components`} component={NotificationElements} />
      <Route path={`${url}/presentational-components`} component={PresentationalElements} />
      <Route render={() => <Redirect to={`${url}/layout-components`} />} />
    </Switch>
  );
};


Components.prototypes = {
    match: shape({
        url: string
    })
};

Components.defaultProps = {
    match: {
        url: '',
    }
};

export default Components
