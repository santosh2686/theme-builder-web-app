import React from "react";

import { ConfigContext } from "@context";

import { ConfigurationWrapper, ComponentConfiguration } from '@local';

import RadioButtonOutput from './RadioButtonOutput/RadioButtonOutput.jsx'

const RadioButton = () => (
  <ConfigContext.Consumer>
      {(context) => {
          const { config, updateConfig, updateSavedItems } = context;
          const { components: { radioButton } } = config;
          return (
            <ConfigurationWrapper
              configValue={radioButton}
              title="Radio Button"
              configMapKey="radioButton"
              parentMapKey="components"
              updateConfig={updateConfig}
              updateSavedItems={updateSavedItems}
              keepOriginal
            >
                <ComponentConfiguration>
                  <RadioButtonOutput />
                </ComponentConfiguration>
            </ConfigurationWrapper>
          );
      }}
  </ConfigContext.Consumer>
);

export default RadioButton;
