import React, { Fragment } from 'react'

import { ConfigContext } from "@context";
import { Panel, Row, Col } from '@common'
import { RadioButton } from '@demo'

const RadioButtonOutput = ({ data }) => (
  <ConfigContext.Consumer>
    {(context) => {
      const { getConfigValue } = context;
      const { default: defaultValues, disabled, hover, label, selected } = data
      const { backgroundColor, borderColor, borderWidth } = defaultValues
      const { typography, textColor, leftSpacing, fontWeight } = label
      const labelTypography = getConfigValue('typography', typography)
      const { fontSize, lineHeight, letterSpacing } = labelTypography;

      const labelStyle = {
        color: getConfigValue('colors', textColor),
        fontWeight: getConfigValue('fontWeight', fontWeight),
        fontSize,
        lineHeight,
        letterSpacing,
        marginLeft: getConfigValue('spacing', leftSpacing),
      }

      return (
        <Fragment>
          <Panel title="Label" classes="mar-b-15 pad-b-15">
            <div style={{
              color: getConfigValue('colors', textColor),
              fontWeight: getConfigValue('fontWeight', fontWeight),
              fontSize,
              lineHeight,
              letterSpacing,
              marginLeft: getConfigValue('spacing', leftSpacing),
            }}>
              Radio button label
            </div>
          </Panel>
          <Panel title="Output">
            <Row>
              <Col col={{ xs: 3 }} classes="pad-b-15">
                <RadioButton
                  labelStyle={labelStyle}
                  label="Default"
                  styleData={{
                    backgroundColor: getConfigValue('colors', backgroundColor),
                    border: borderWidth && `${borderWidth} solid ${getConfigValue('colors', borderColor)}`,
                  }}
                />
              </Col>
              <Col col={{ xs: 3 }} classes="pad-b-15">
                <RadioButton
                  labelStyle={labelStyle}
                  label="Hover"
                  styleData={{
                    backgroundColor: getConfigValue('colors', hover.backgroundColor),
                    border: hover.borderWidth && `${hover.borderWidth} solid ${getConfigValue('colors', hover.borderColor)}`,
                  }}
                />
              </Col>
              <Col col={{ xs: 3 }} classes="pad-b-15">
                <RadioButton
                  fillStyle={{
                    backgroundColor: getConfigValue('colors', selected.fillColor)
                  }}
                  labelStyle={labelStyle}
                  label="Checked"
                  styleData={{
                    backgroundColor: getConfigValue('colors', selected.backgroundColor),
                    border: selected.borderWidth && `${selected.borderWidth} solid ${getConfigValue('colors', selected.borderColor)}`,
                  }}
                />
              </Col>
              <Col col={{ xs: 3 }} classes="pad-b-15">
                <RadioButton
                  fillStyle={{
                    backgroundColor: getConfigValue('colors', disabled.fillColor)
                  }}
                  labelStyle={labelStyle}
                  label="Disabled"
                  styleData={{
                    backgroundColor: getConfigValue('colors', disabled.backgroundColor),
                    border: disabled.borderWidth && `${disabled.borderWidth} solid ${getConfigValue('colors', disabled.borderColor)}`,
                  }}
                />
              </Col>
            </Row>
          </Panel>
        </Fragment>
      )
    }}
  </ConfigContext.Consumer>
)

export default RadioButtonOutput
