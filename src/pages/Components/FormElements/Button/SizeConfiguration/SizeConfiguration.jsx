import React, { PureComponent } from "react";
import update from "immutability-helper"

import { arrayToObject, objectToArray } from "@utils";

import { AddRemoveItem } from "@local";

import Size from "../Size/Size.jsx";

class SizeConfiguration extends PureComponent {
  state = {
    isLoading: true,
    data: []
  }
  componentDidMount() {
    const { data } = this.props;
    const updatedData = objectToArray(data)
    this.setState({
      data: updatedData,
      isLoading: false,
    })
  }

  updateState = (valueObj) => {
    this.setState(prevState => update(prevState, {
      data: valueObj
    }), () => {
      const { data } = this.state;
      const { changeHandler } = this.props;
      changeHandler({
        size: {
          $set: arrayToObject(data)
        }
      })
    })
  }

  addHandler = () => {
    this.updateState({
      $push: [
        {
          name: "",
          value: {},
        },
      ],
    });
  };

  removeHandler = (index) => {
    this.updateState({
      $splice: [[index, 1]],
    });
  };

  changeHandler = (data, index) => {
    this.updateState({
      [index]: {
        value: {
          $merge: data
        }
      }
    })
  }
  render() {
    const { data, isLoading } = this.state;
    if (isLoading) {
      return null
    }
    return (
      <AddRemoveItem
        itemList={data}
        addHandler={this.addHandler}
        removeHandler={this.removeHandler}
      >
      <Size changeHandler={this.changeHandler} />
      </AddRemoveItem>
    );
  }
}

export default SizeConfiguration
