import React, { Fragment, PureComponent } from "react";

import { Button, TextInput, Row, Col, Modal } from '@common';

import CategoryConfigurationModal from "../CategoryConfigurationModal/CategoryConfigurationModal.jsx";

class Category extends PureComponent {
  state = {
    selectedCategory: {},
    showModal: false,
  }

  nameChangeHandler = (data) => {
    const { categoryChangeHandler, index } = this.props;
    categoryChangeHandler(data, index)
  }

  configureCategory = () => {
    const { item } = this.props;
    this.setState({
      selectedCategory: item,
      showModal: true
    })
  }

  closeModal = () => {
    this.setState({
      selectedCategory: {},
      showModal: false,
    })
  }

  render() {
    const { selectedCategory, showModal } = this.state;
    const { item, submitHandler, common, size } = this.props;
    const { name } = item;
    return (
      <Fragment>
        <Row classes="align-end">
          <Col col={{ xs: 12, sm: 12, md: 12, lg: 6 }}>
            <TextInput
              name="name"
              label="Category Name"
              value={name}
              changeHandler={this.nameChangeHandler}
            />
          </Col>
          <Col col={{ xs: 12, sm: 12, md: 12, lg: 6 }}>
            <Button clickHandler={this.configureCategory} disabled={!name}>
              Configure
            </Button>
          </Col>
        </Row>
        <Modal
          show={showModal}
          closeHandler={this.closeModal}
          size="large"
        >
          <CategoryConfigurationModal
            selectedCategory={selectedCategory}
            submitHandler={submitHandler}
            closeModal={this.closeModal}
            common={common}
            size={size}
          />
        </Modal>
      </Fragment>
    );
  }
}

export default Category
