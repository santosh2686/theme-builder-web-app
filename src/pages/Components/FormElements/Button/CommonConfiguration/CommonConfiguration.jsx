import React, { Fragment, PureComponent }  from 'react';

import { TextInput, Row, Col } from '@common';
import { ConfigSelection } from '@local';

class CommonConfiguration extends PureComponent {
  changeHandler = (data) => {
    const { changeHandler } = this.props;
    changeHandler({
      common: {
        $merge: data
      }
    })
  }

  render() {
    const { data = {} } = this.props;
    const { borderRadius, shadowColorOnHover, disabledOpacity } = data;
    return (
      <Fragment>
        <Row>
          <Col col={{ xs: 12, sm: 12, md: 12, lg: 6}} classes="pad-b-10">
            <TextInput
              name="borderRadius"
              value={borderRadius}
              label="Border radius"
              changeHandler={this.changeHandler}
              attributes={{
              placeholder: 'e.g.: 4px'
            }}
            />
          </Col>
          <Col col={{ xs: 12, sm: 12, md: 12, lg: 6}} classes="pad-b-10">
            <ConfigSelection
              configKey="colors"
              label="Shadow color on hover"
              name="shadowColorOnHover"
              selected={shadowColorOnHover}
              changeHandler={this.changeHandler}
            />
          </Col>
        </Row>
        <Row>
          <Col col={{ xs: 12, sm: 12, md: 12, lg: 6}} classes="pad-b-10">
            <TextInput
              name="disabledOpacity"
              value={disabledOpacity}
              label="Disabled opacity"
              changeHandler={this.changeHandler}
              attributes={{
                placeholder: 'e.g.: 0.5'
              }}
            />
          </Col>
        </Row>
      </Fragment>
    )
  }
}

export default CommonConfiguration
