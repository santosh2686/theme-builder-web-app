import React from "react";

import { ConfigContext } from "@context";
import { Row, Col } from "@common";
import { MapCssModules } from "@utils";
import { Button } from "@demo";

const ButtonView = ({ common, data, size }) => {
  const { default: defaultSize } = size;
  const { borderRadius, shadowColorOnHover, disabledOpacity } = common;
  const { height, horizontalSpacing, typography, fontWeight } = defaultSize

  const {
    backgroundColor,
    textColor,
    loaderBackground,
    borderWidth,
    borderColor,
    hover = {}
  } = data;
  const {
    backgroundColor: backgroundColorHover,
    textColor: textColorHover,
    borderWidth: borderWidthHover,
    borderColor: borderColorHover
  } = hover;
  return (
    <ConfigContext.Consumer>
      {(context) => {
        const { getConfigValue } = context;
        const buttonTypography = getConfigValue('typography', typography)
        const { fontSize, lineHeight, letterSpacing } = buttonTypography;
        return (
          <Row>
            <Col col={{ xs: 12, sm: 12, md: 12, lg: 3 }} classes="pad-b-15">
              <Button data={{
                color: getConfigValue('colors', textColor),
                backgroundColor: getConfigValue('colors', backgroundColor),
                borderRadius,
                height,
                padding: `0 ${horizontalSpacing}`,
                fontWeight: getConfigValue('fontWeight', fontWeight),
                fontSize,
                lineHeight,
                letterSpacing,
                border: borderWidth && `${borderWidth} solid ${getConfigValue('colors', borderColor)}`,
              }}
              />
            </Col>
            <Col col={{ xs: 12, sm: 12, md: 12, lg: 3 }} classes="pad-b-15">
              <Button data={{
                color: getConfigValue('colors', textColorHover),
                backgroundColor: getConfigValue('colors', backgroundColorHover),
                borderRadius,
                height,
                padding: `0 ${horizontalSpacing}`,
                fontWeight: getConfigValue('fontWeight', fontWeight),
                fontSize,
                lineHeight,
                letterSpacing,
                border: borderWidth && `${borderWidthHover} solid ${getConfigValue('colors', borderColorHover)}`,
                boxShadow: `0 2px 10px 0 ${getConfigValue('colors', shadowColorOnHover)}`,
              }}
              />
            </Col>
            <Col col={{ xs: 12, sm: 12, md: 12, lg: 3 }} classes="pad-b-15">
              <Button data={{
                color: getConfigValue('colors', textColor),
                backgroundColor: getConfigValue('colors', backgroundColor),
                borderRadius,
                height,
                padding: `0 ${horizontalSpacing}`,
                fontWeight: getConfigValue('fontWeight', fontWeight),
                fontSize,
                lineHeight,
                letterSpacing,
                opacity: disabledOpacity,
                border: borderWidth && `${borderWidth} solid ${getConfigValue('colors', borderColor)}`,
              }}
              />
            </Col>
            <Col col={{ xs: 12, sm: 12, md: 12, lg: 3 }} classes="pad-b-15">
              <Button isLoading data={{
                color: getConfigValue('colors', backgroundColor),
                backgroundColor: getConfigValue('colors', backgroundColor),
                borderRadius,
                loaderBg: getConfigValue('colors', loaderBackground),
                height,
                padding: `0 ${horizontalSpacing}`,
                fontWeight: getConfigValue('fontWeight', fontWeight),
                fontSize,
                lineHeight,
                letterSpacing,
                border: borderWidth && `${borderWidth} solid ${getConfigValue('colors', borderColor)}`,
              }}
              />
            </Col>
          </Row>
        )
      }}
    </ConfigContext.Consumer>

  )
}

export default MapCssModules(ButtonView)
