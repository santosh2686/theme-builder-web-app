import React from "react";

import { Row, Col, Text, Panel } from "@common";

import ButtonOutput from "../ButtonOutput/ButtonOutput.jsx";

const ButtonOutputWrapper = ({ category, common, size }) => {
  return (
   <div>
     {Object.keys(category).map((categoryName) => {
       return (
         <Panel key={categoryName} title={`${categoryName} button`} classes="flex-1 mar-b-15">
           <Row>
             <Col col={{ xs: 12, sm: 12, md: 12, lg: 3 }}>
               <Text tag="div" color="gray" classes="pad-b-5">Basic</Text>
             </Col>
             <Col col={{ xs: 12, sm: 12, md: 12, lg: 3 }}>
               <Text tag="div" color="gray" classes="pad-b-5">Hover</Text>
             </Col>
             <Col col={{ xs: 12, sm: 12, md: 12, lg: 3 }}>
               <Text tag="div" color="gray" classes="pad-b-5">Disabled</Text>
             </Col>
             <Col col={{ xs: 12, sm: 12, md: 12, lg: 3 }}>
               <Text tag="div" color="gray" classes="pad-b-5">Loading</Text>
             </Col>
           </Row>
           <ButtonOutput
             key={categoryName}
             item={category[categoryName]}
             common={common}
             size={size}
             label={categoryName}
           />
         </Panel>
       )
     })}
   </div>
  )
}

export default ButtonOutputWrapper
