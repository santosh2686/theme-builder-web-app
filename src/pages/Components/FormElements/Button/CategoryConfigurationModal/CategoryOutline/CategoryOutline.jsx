import React, { Fragment, PureComponent } from "react";

import { ConfigContext } from "@context";
import { Row, Col, Panel, TextInput } from "@common";
import { ConfigSelection } from "@local";
import { Button } from "@demo";

class CategoryOutline extends PureComponent {
  changeHandler = (data) => {
    const { changeHandler } = this.props;
    changeHandler({
      outline: {
        $merge: data
      }
    })
  }

  hoverChangeHandler = (data) => {
    const { changeHandler } = this.props;
    changeHandler({
      outline: {
        hover :{
          $merge: data
        }
      }
    })
  }
  render() {
    const { common, size, data = {} } = this.props;
    const { default: defaultSize } = size;
    const { borderRadius, shadowColorOnHover, disabledOpacity } = common;
    const { height, horizontalSpacing, typography, fontWeight } = defaultSize
    const {
      backgroundColor,
      textColor,
      borderWidth,
      borderColor,
      loaderBackground,
      hover = {}
    } = data;
    const {
      backgroundColor: backgroundColorHover,
      textColor: textColorHover,
      borderWidth: borderWidthHover,
      borderColor: borderColorHover
    } = hover;
    return (
      <ConfigContext.Consumer>
        {(context) => {
          const { getConfigValue } = context;
          const buttonTypography = getConfigValue('typography', typography)
          const { fontSize, lineHeight, letterSpacing } = buttonTypography;
          return (
            <Fragment>
              <Row>
                <Col col={{ xs: 12, sm: 12, md: 12, lg: 6 }}>
                  <Panel title="Outline" classes="mar-b-15">
                    <Row>
                      <Col col={{ xs: 12, sm: 12, md: 12, lg: 6 }} classes="pad-b-10">
                        <ConfigSelection
                          configKey="colors"
                          label="Background color"
                          name="backgroundColor"
                          selected={backgroundColor}
                          changeHandler={this.changeHandler}
                        />
                      </Col>
                      <Col col={{ xs: 12, sm: 12, md: 12, lg: 6 }} classes="pad-b-10">
                        <ConfigSelection
                          configKey="colors"
                          label="Text color"
                          name="textColor"
                          selected={textColor}
                          changeHandler={this.changeHandler}
                        />
                      </Col>
                    </Row>
                    <Row>
                      <Col col={{ xs: 12, sm: 12, md: 12, lg: 6 }} classes="pad-b-10">
                        <TextInput
                          name="borderWidth"
                          label="Border width"
                          value={borderWidth}
                          changeHandler={this.changeHandler}
                          attributes={{
                            placeholder: "e.g.: 1px"
                          }}
                        />
                      </Col>
                      <Col col={{ xs: 12, sm: 12, md: 12, lg: 6 }} classes="pad-b-10">
                        <ConfigSelection
                          configKey="colors"
                          label="Border color"
                          name="borderColor"
                          selected={borderColor}
                          changeHandler={this.changeHandler}
                        />
                      </Col>
                    </Row>
                    <Row>
                      <Col col={{ xs: 12, sm: 12, md: 12, lg: 6 }} classes="pad-b-10">
                        <ConfigSelection
                          configKey="colors"
                          label="Loader background color"
                          name="loaderBackground"
                          selected={loaderBackground}
                          changeHandler={this.changeHandler}
                        />
                      </Col>
                    </Row>
                  </Panel>
                </Col>
                <Col col={{ xs: 12, sm: 12, md: 12, lg: 6 }} classes="flex flex-column">
                  <Panel title="Outline output" classes="mar-b-15 flex-1">
                    <Row>
                      <Col col={{ xs: 12, sm: 12, md: 12, lg: 4 }}>
                        <Button data={{
                          color: getConfigValue('colors', textColor),
                          backgroundColor: getConfigValue('colors', backgroundColor),
                          borderRadius,
                          border: `${borderWidth} solid ${getConfigValue('colors', borderColor)}`,
                          height,
                          padding: `0 ${horizontalSpacing}`,
                          fontWeight: getConfigValue('fontWeight', fontWeight),
                          fontSize,
                          lineHeight,
                          letterSpacing
                        }}
                        />
                      </Col>
                      <Col col={{ xs: 12, sm: 12, md: 12, lg: 4 }}>
                        <Button data={{
                          color: getConfigValue('colors', textColor),
                          backgroundColor: getConfigValue('colors', backgroundColor),
                          borderRadius,
                          opacity: disabledOpacity,
                          border: `${borderWidth} solid ${getConfigValue('colors', borderColor)}`,
                          height,
                          padding: `0 ${horizontalSpacing}`,
                          fontWeight: getConfigValue('fontWeight', fontWeight),
                          fontSize,
                          lineHeight,
                          letterSpacing
                        }}
                        />
                      </Col>
                      <Col col={{ xs: 12, sm: 12, md: 12, lg: 4 }}>
                        <Button isLoading data={{
                          color: getConfigValue('colors', backgroundColor),
                          backgroundColor: getConfigValue('colors', backgroundColor),
                          borderRadius,
                          loaderBg: getConfigValue('colors', loaderBackground),
                          border: `${borderWidth} solid ${getConfigValue('colors', borderColor)}`,
                          height,
                          padding: `0 ${horizontalSpacing}`,
                          fontWeight: getConfigValue('fontWeight', fontWeight),
                          fontSize,
                          lineHeight,
                          letterSpacing
                        }}
                        />
                      </Col>
                    </Row>
                  </Panel>
                </Col>
              </Row>
              <Row>
                <Col col={{ xs: 12, sm: 12, md: 12, lg: 6 }}>
                  <Panel title="Outline hover state" classes="mar-b-15">
                    <Row>
                      <Col col={{ xs: 12, sm: 12, md: 12, lg: 6 }} classes="pad-b-10">
                        <ConfigSelection
                          configKey="colors"
                          label="Background color"
                          name="backgroundColor"
                          selected={backgroundColorHover}
                          changeHandler={this.hoverChangeHandler}
                        />
                      </Col>
                      <Col col={{ xs: 12, sm: 12, md: 12, lg: 6 }} classes="pad-b-10">
                        <ConfigSelection
                          configKey="colors"
                          label="Text color"
                          name="textColor"
                          selected={textColorHover}
                          changeHandler={this.hoverChangeHandler}
                        />
                      </Col>
                    </Row>
                    <Row>
                      <Col col={{ xs: 12, sm: 12, md: 12, lg: 6 }} classes="pad-b-10">
                        <TextInput
                          name="borderWidth"
                          label="Border width"
                          value={borderWidthHover}
                          changeHandler={this.hoverChangeHandler}
                          attributes={{
                            placeholder: "e.g.: 1px"
                          }}
                        />
                      </Col>
                      <Col col={{ xs: 12, sm: 12, md: 12, lg: 6 }} classes="pad-b-10">
                        <ConfigSelection
                          configKey="colors"
                          label="Border color"
                          name="borderColor"
                          selected={borderColorHover}
                          changeHandler={this.hoverChangeHandler}
                        />
                      </Col>
                    </Row>
                  </Panel>
                </Col>
                <Col col={{ xs: 12, sm: 12, md: 12, lg: 6 }} classes="flex flex-column">
                  <Panel title="Outline hover state output" classes="mar-b-15 flex-1">
                    <Button data={{
                      color: getConfigValue('colors', textColorHover),
                      backgroundColor: getConfigValue('colors', backgroundColorHover),
                      borderRadius,
                      boxShadow: `0 2px 10px 0 ${getConfigValue('colors', shadowColorOnHover)}`,
                      border: `${borderWidthHover} solid ${getConfigValue('colors', borderColorHover)}`,
                      height,
                      padding: `0 ${horizontalSpacing}`,
                      fontWeight: getConfigValue('fontWeight', fontWeight),
                      fontSize,
                      lineHeight,
                      letterSpacing
                    }}
                    />
                  </Panel>
                </Col>
              </Row>
            </Fragment>
          )
        }}
      </ConfigContext.Consumer>
    )
  }
}

export default CategoryOutline
