import React, { Fragment, PureComponent } from "react";
import update from "immutability-helper";

import { Layout, Button, Icon, Text } from "@common";

import CategoryBasic from "./CategoryBasic/CategoryBasic.jsx";
import CategoryOutline from "./CategoryOutline/CategoryOutline.jsx";

class CategoryConfigurationModal extends PureComponent {
  state = {
    data: {},
    step: 1
  }

  componentDidMount() {
    const { selectedCategory } = this.props;
    const { value } = selectedCategory;
    this.setState({
      data: value
    })
  }

  nextHandler = () => {
    this.setState({
      step: 2
    })
  }

  prevHandler = () => {
    this.setState({
      step: 1
    })
  }

  submitHandler = () => {
    const { selectedCategory, submitHandler, closeModal } = this.props;
    const { name } = selectedCategory
    const { data } = this.state;
    submitHandler({
      category: {
        [name]: {
          $merge: data
        }
      }
    })
    closeModal()
  }

  changeHandler = (valueObj) => {
    this.setState(prevState => update(prevState, {
      data: valueObj
    }))
  }

  render() {
    const { step, data } = this.state;
    const { closeModal, selectedCategory, common, size } = this.props;
    const { name } = selectedCategory
    const { basic, outline } = data;
    return (
      <Fragment>
        <Layout flex={{ align: 'center', justify: 'space-between' }} pad={{ tb: 5, lr: 10 }}>
          <h4>
            <Text classes="capitalize">{name} &nbsp;</Text>
            category configuration
          </h4>
          <Button category="clear" clickHandler={closeModal}>
            <Icon name="close" />
          </Button>
        </Layout>
        <Layout pad={{ t: 15, l: 15, r: 15 }} border={{ tb: 'gray-light'}} bgColor="gray-light" classes="overflow-auto">
          {step === 1 && (
            <CategoryBasic common={common} size={size} data={basic} changeHandler={this.changeHandler} />
          )}
          {step === 2 && (
            <CategoryOutline common={common} size={size} data={outline} changeHandler={this.changeHandler} />
          )}
        </Layout>
        <Layout classes="text-right" pad={{ tb: 5, lr: 10 }}>
          {step === 2 && (<Button category="default" clickHandler={this.prevHandler}>Prev</Button>)}
          {step === 1 && (<Button clickHandler={this.nextHandler}>Next</Button>)}
          {step === 2 && (<Button classes="mar-l-15" clickHandler={this.submitHandler}>Submit</Button>)}
        </Layout>
      </Fragment>
    );
  }
}

export default CategoryConfigurationModal
