import React, { PureComponent } from "react";
import update from "immutability-helper"

import { arrayToObject, objectToArray } from "@utils";

import { AddRemoveItem } from "@local";

import Category from "../Category/Category"

class CategoryConfiguration extends PureComponent {
  state = {
    isLoading: true,
    data: []
  }

  componentDidMount() {
    const { data } = this.props;
    const updatedData = objectToArray(data)
    this.setState({
      data: updatedData,
      isLoading: false,
    })
  }

  updateState = (valueObj) => {
    this.setState(prevState => update(prevState, {
      data: valueObj
    }), () => {
      const { data } = this.state;
      const { changeHandler } = this.props;
      changeHandler({
        category: {
          $set: arrayToObject(data)
        }
      })
    })
  }

  addHandler = () => {
    this.updateState({
      $push: [
        {
          name: "",
          value: {
            basic: {
              backgroundColor: "gray",
              textColor: "white",
              loaderBackground: "white",
              hover: {
                backgroundColor: "gray",
                textColor: "white"
              }
            },
            outline: {
              backgroundColor: "white",
              textColor: "gray",
              borderWidth: "1px",
              borderColor: "gray",
              loaderBackground: "gray",
              hover: {
                backgroundColor: "white",
                textColor: "gray",
                borderWidth: "2px",
                borderColor: "gray",
              }
            }
          },
        },
      ],
    });
  };

  removeHandler = (index) => {
    this.updateState({
      $splice: [[index, 1]],
    });
  };

  categoryChangeHandler = (data, index) => {
    this.updateState({
      [index]: {
        $merge: data
      }
    })
  }

  render() {
    const { data, isLoading } = this.state;
    const { changeHandler, common, size } = this.props;
    if (isLoading) {
      return null
    }
    return (
      <AddRemoveItem
        itemList={data}
        addHandler={this.addHandler}
        removeHandler={this.removeHandler}
      >
        <Category
          categoryChangeHandler={this.categoryChangeHandler}
          submitHandler={changeHandler}
          common={common}
          size={size}
        />
      </AddRemoveItem>
    );
  }
}

export default CategoryConfiguration
