import React, { Fragment, PureComponent } from "react";

import { TextInput, Row, Col } from "@common";

import { ConfigSelection } from "@local";

class Size extends PureComponent {
  changeHandler = (data) => {
    const { changeHandler, index } = this.props;
    changeHandler(data, index)
  }

  render() {
    const { item } = this.props;
    const { name, value } = item;
    const { typography, fontWeight, height, horizontalSpacing } = value;
    return (
      <Fragment>
        <Row>
          <Col col={{ xs: 12, sm: 12, md: 12, lg: 6 }} classes="pad-b-10">
            <TextInput
              name="name"
              label="Size Name"
              value={name}
              changeHandler={this.changeHandler}
              attributes={{
                placeholder: 'e.g.: default'
              }}
            />
          </Col>
          <Col col={{ xs: 12, sm: 12, md: 12, lg: 6 }} classes="pad-b-10">
            <ConfigSelection
              configKey="typography"
              label="Typography"
              name="typography"
              selected={typography}
              changeHandler={this.changeHandler}
            />
          </Col>
        </Row>
        <Row>
          <Col col={{ xs: 12, sm: 12, md: 12, lg: 6 }} classes="pad-b-10">
            <TextInput
              name="height"
              label="Height"
              value={height}
              changeHandler={this.changeHandler}
              attributes={{
                placeholder: 'e.g.: 40px'
              }}
            />
          </Col>
          <Col col={{ xs: 12, sm: 12, md: 12, lg: 6 }} classes="pad-b-10">
            <ConfigSelection
              configKey="fontWeight"
              label="Font weight"
              name="fontWeight"
              selected={fontWeight}
              changeHandler={this.changeHandler}
            />
          </Col>
        </Row>
        <Row>
          <Col col={{ xs: 12, sm: 12, md: 12, lg: 6 }}>
            <TextInput
              name="horizontalSpacing"
              label="Horizontal Spacing"
              value={horizontalSpacing}
              changeHandler={this.changeHandler}
              attributes={{
                placeholder: 'e.g.: 20px'
              }}
            />
          </Col>
        </Row>
      </Fragment>
    );
  }
}

export default Size
