import React, { PureComponent } from "react";

import { MapCssModules } from '@utils';
import { Panel, Row, Col } from "@common";

import CommonConfiguration from '../CommonConfiguration/CommonConfiguration.jsx'
import CategoryConfiguration from '../CategoryConfiguration/CategoryConfiguration.jsx'
import SizeConfiguration from '../SizeConfiguration/SizeConfiguration.jsx'

import ButtonOutputWrapper from "../ButtonOutputWrapper/ButtonOutputWrapper.jsx";

class ButtonConfiguration extends PureComponent {
  changeHandler = (data) => {
    const { changeHandler, updateSavedItems, configMapKey } = this.props;
    changeHandler(data);
    updateSavedItems(configMapKey, false);
  };

  render() {
    const { data } = this.props;
    const { common = {}, category = {}, size = {} } = data;
    return (
      <Row>
        <Col col={{ xs: 12, sm: 12, md: 12, lg: 6 }}  classes="mar-b-10 flex flex-column">
          <Panel title="Common" classes="flex-1 mar-b-15">
              <CommonConfiguration
                data={common}
                changeHandler={this.changeHandler}
              />
          </Panel>
          <Panel title="Categories" classes="flex-1 mar-b-15 pad-b-10">
              <CategoryConfiguration
                size={size}
                common={common}
                data={category}
                changeHandler={this.changeHandler}
              />
          </Panel>
          <Panel title="Sizes" classes="flex-1 mar-b-15 pad-b-10">
              <SizeConfiguration
                common={common}
                data={size}
                changeHandler={this.changeHandler}
              />
          </Panel>
        </Col>
        <Col col={{ xs: 12, sm: 12, md: 12, lg: 6 }} classes="mar-b-10 flex flex-column">
          <ButtonOutputWrapper
            common={common}
            category={category}
            size={size}
          />
        </Col>
      </Row>
    );
  }
}

export default MapCssModules(ButtonConfiguration);
