import React from "react";

import { ConfigContext } from "@context";

import { ConfigurationWrapper } from "@local";

import ButtonConfiguration from "./ButtonConfiguration/ButtonConfiguration.jsx";

const Button = () => (
  <ConfigContext.Consumer>
      {(context) => {
          const { config, updateConfig, updateSavedItems } = context;
          const { components: { button } } = config;
          return (
            <ConfigurationWrapper
              configValue={button}
              title="Button"
              configMapKey="button"
              parentMapKey="components"
              updateConfig={updateConfig}
              updateSavedItems={updateSavedItems}
              keepOriginal
            >
                <ButtonConfiguration
                  configMapKey="button"
                  updateSavedItems={updateSavedItems}
                />
            </ConfigurationWrapper>
          );
      }}
  </ConfigContext.Consumer>
);

export default Button;
