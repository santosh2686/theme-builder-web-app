import React from "react";
import ButtonView from "../ButtonView/ButtonView.jsx";

const ButtonOutput = ({ common, item, size, label }) => {
  const { basic, outline } = item
  return(
    <div>
      <ButtonView label={label} data={basic} common={common} size={size} />
      <ButtonView label={label} data={outline} common={common} size={size} />
    </div>
  )
}

export default ButtonOutput
