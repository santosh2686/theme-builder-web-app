import React from "react";
import { shape, string } from "prop-types";
import { Route, Switch, Redirect } from "react-router-dom";

import Button from './Button/Button.jsx';
import CheckBox from './CheckBox/CheckBox.jsx';
import DropDown from './DropDown/DropDown.jsx';
import FileUpload from './FileUpload/FileUpload.jsx';
import RadioButton from './RadioButton/RadioButton.jsx';
import RangeSlider from './RangeSlider/RangeSlider.jsx';
import TextInput from './TextInput/TextInput.jsx';
import ToggleButton from './ToggleButton/ToggleButton.jsx';

const FormElements = ({
  match: { url }
}) => {
  return (
    <Switch>
      <Route exact path={url} render={() => <Redirect to={`${url}/button`} />} />
      <Route exact path={`${url}/button`} component={Button} />
      <Route exact path={`${url}/check-box`} component={CheckBox} />
      <Route exact path={`${url}/drop-down`} component={DropDown} />
      <Route exact path={`${url}/file-upload`} component={FileUpload} />
      <Route exact path={`${url}/radio-button`} component={RadioButton} />
      <Route exact path={`${url}/range-slider`} component={RangeSlider} />
      <Route exact path={`${url}/text-input`} component={TextInput} />
      <Route exact path={`${url}/toggle-button`} component={ToggleButton} />
      <Route render={() => <Redirect to={`${url}/button`} />} />
    </Switch>
  );
};

FormElements.prototypes = {
  match: shape({
    url: string
  })
};

FormElements.defaultProps = {
  match: {
    url: '',
  }
};

export default FormElements
