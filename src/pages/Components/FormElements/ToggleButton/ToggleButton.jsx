import React from 'react';

import { ConfigContext } from '@context'
import { ComponentConfiguration, ConfigurationWrapper } from '@local'

import ToggleButtonOutput from './ToggleButtonOutput/ToggleButtonOutput.jsx'

const ToggleButton = () => (
  <ConfigContext.Consumer>
      { (context) => {
          const { config, updateConfig, updateSavedItems } = context;
          const { components: { toggleButton } } = config;
          return (
            <ConfigurationWrapper
              configValue={toggleButton}
              title="Toggle button"
              configMapKey="toggleButton"
              parentMapKey="components"
              updateConfig={updateConfig}
              updateSavedItems={updateSavedItems}
              keepOriginal
            >
                <ComponentConfiguration>
                  <ToggleButtonOutput />
                </ComponentConfiguration>
            </ConfigurationWrapper>
          );
      } }
  </ConfigContext.Consumer>
)

export default ToggleButton
