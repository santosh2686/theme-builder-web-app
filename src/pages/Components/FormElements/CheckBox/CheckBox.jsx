import React from 'react';

import { ConfigContext } from '@context'

import { ComponentConfiguration, ConfigurationWrapper } from '@local'

import CheckBoxOutput from './CheckBoxOutput/CheckBoxOutput.jsx'

const CheckBox = () => (
  <ConfigContext.Consumer>
    { (context) => {
      const { config, updateConfig, updateSavedItems } = context;
      const { components: { checkBox } } = config;
      return (
        <ConfigurationWrapper
          configValue={checkBox}
          title="Check box"
          configMapKey="checkBox"
          parentMapKey="components"
          updateConfig={updateConfig}
          updateSavedItems={updateSavedItems}
          keepOriginal
        >
          <ComponentConfiguration>
            <CheckBoxOutput />
          </ComponentConfiguration>
        </ConfigurationWrapper>
      );
    } }
  </ConfigContext.Consumer>
)

export default CheckBox
