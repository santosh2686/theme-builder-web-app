import React, { Fragment } from 'react'

import { ConfigContext } from "@context";
import { Panel, Row, Col } from '@common'
import { CheckBox } from '@demo'

const CheckBoxOutput = ({ data }) => (
  <ConfigContext.Consumer>
    {(context) => {
      const { getConfigValue } = context;
      const { common, default: defaultValues, disabled, hover, label, selected } = data
      const { backgroundColor, borderColor, borderWidth } = defaultValues
      const { borderRadius, width, height } = common
      const { typography, textColor, leftSpacing, fontWeight } = label
      const labelTypography = getConfigValue('typography', typography)
      const { fontSize, lineHeight, letterSpacing } = labelTypography;

      const labelStyle = {
        color: getConfigValue('colors', textColor),
        fontWeight: getConfigValue('fontWeight', fontWeight),
        fontSize,
        lineHeight,
        letterSpacing,
        marginLeft: getConfigValue('spacing', leftSpacing),
      }

      return (
        <Fragment>
          <Panel title="Label" classes="mar-b-15 pad-b-15">
            <div style={{
              color: getConfigValue('colors', textColor),
              fontWeight: getConfigValue('fontWeight', fontWeight),
              fontSize,
              lineHeight,
              letterSpacing,
              marginLeft: getConfigValue('spacing', leftSpacing),
            }}>
              Checkbox label
            </div>
          </Panel>
          <Panel title="Output">
            <Row>
              <Col col={{ xs: 3 }} classes="pad-b-15">
                <CheckBox
                  labelStyle={labelStyle}
                  label="Default"
                  styleData={{
                    backgroundColor: getConfigValue('colors', backgroundColor),
                    border: borderWidth && `${borderWidth} solid ${getConfigValue('colors', borderColor)}`,
                    borderRadius: borderRadius,
                    width,
                    height
                  }}
                />
              </Col>
              <Col col={{ xs: 3 }} classes="pad-b-15">
                <CheckBox
                  labelStyle={labelStyle}
                  label="Hover"
                  styleData={{
                    backgroundColor: getConfigValue('colors', hover.backgroundColor),
                    border: hover.borderWidth && `${hover.borderWidth} solid ${getConfigValue('colors', hover.borderColor)}`,
                    borderRadius: borderRadius,
                    width,
                    height
                  }}
                />
              </Col>
              <Col col={{ xs: 3 }} classes="pad-b-15">
                <CheckBox
                  tickStyle={{
                    borderColor: `transparent ${getConfigValue('colors', selected.tickColor)} ${getConfigValue('colors', selected.tickColor)} transparent`
                  }}
                  labelStyle={labelStyle}
                  label="Checked"
                  styleData={{
                    backgroundColor: getConfigValue('colors', selected.backgroundColor),
                    border: selected.borderWidth && `${selected.borderWidth} solid ${getConfigValue('colors', selected.borderColor)}`,
                    borderRadius: borderRadius,
                    width,
                    height
                  }}
                />
              </Col>
              <Col col={{ xs: 3 }} classes="pad-b-15">
                <CheckBox
                  tickStyle={{
                    borderColor: `transparent ${getConfigValue('colors', disabled.tickColor)} ${getConfigValue('colors', disabled.tickColor)} transparent`
                  }}
                  labelStyle={labelStyle}
                  label="Disabled"
                  styleData={{
                    backgroundColor: getConfigValue('colors', disabled.backgroundColor),
                    border: disabled.borderWidth && `${disabled.borderWidth} solid ${getConfigValue('colors', disabled.borderColor)}`,
                    borderRadius: borderRadius,
                    width,
                    height
                  }}
                />
              </Col>
            </Row>
          </Panel>
        </Fragment>
      )
    }}
  </ConfigContext.Consumer>
)

export default CheckBoxOutput
