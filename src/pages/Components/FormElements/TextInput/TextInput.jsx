import React from 'react';
import { ConfigContext } from "@context/index"
import { ComponentConfiguration, ConfigurationWrapper } from "@local/index"

import TextInputOutput from './TextInputOutput/TextInputOutput.jsx';

const TextInput = () => (
  <ConfigContext.Consumer>
      { (context) => {
          const { config, updateConfig, updateSavedItems } = context;
          const { components: { inputField } } = config;
          return (
            <ConfigurationWrapper
              configValue={inputField}
              title="Input field"
              configMapKey="inputField"
              parentMapKey="components"
              updateConfig={updateConfig}
              updateSavedItems={updateSavedItems}
              keepOriginal
            >
                <ComponentConfiguration>
                  <TextInputOutput />
                </ComponentConfiguration>
            </ConfigurationWrapper>
          );
      } }
  </ConfigContext.Consumer>
)

export default TextInput
