import React from 'react';
import { ConfigContext } from "@context/index"
import { ComponentConfiguration, ConfigurationWrapper } from "@local/index"

import DropDownOutput from './DropDownOutput/DropDownOutput.jsx';

const DropDown = () => (
  <ConfigContext.Consumer>
      { (context) => {
          const { config, updateConfig, updateSavedItems } = context;
          const { components: { dropDown } } = config;
          return (
            <ConfigurationWrapper
              configValue={dropDown}
              title="Drop down"
              configMapKey="dropDown"
              parentMapKey="components"
              updateConfig={updateConfig}
              updateSavedItems={updateSavedItems}
              keepOriginal
            >
                <ComponentConfiguration>
                  <DropDownOutput />
                </ComponentConfiguration>
            </ConfigurationWrapper>
          );
      } }
  </ConfigContext.Consumer>
)

export default DropDown
