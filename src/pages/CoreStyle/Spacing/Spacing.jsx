import React, { Fragment } from "react";

import { ConfigContext } from "@context";

import { ConfigurationWrapper, Configuration, ConfigurationItem } from "@local";

const Spacing = () => {
  return (
    <Fragment>
      <ConfigContext.Consumer>
        {(context) => {
          const { config, updateConfig, updateSavedItems } = context;
          const { spacing } = config;
          return (
            <ConfigurationWrapper
              configValue={spacing}
              title="Spacings"
              configMapKey="spacing"
              updateConfig={updateConfig}
              updateSavedItems={updateSavedItems}
            >
              <Configuration>
                <ConfigurationItem
                  namePlaceholder="4"
                  valuePlaceholder="4px"
                  configMapKey="spacing"
                  updateSavedItems={updateSavedItems}
                />
              </Configuration>
            </ConfigurationWrapper>
          );
        }}
      </ConfigContext.Consumer>
    </Fragment>
  );
};

export default Spacing;
