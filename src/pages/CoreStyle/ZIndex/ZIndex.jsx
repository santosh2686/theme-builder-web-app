import React, { Fragment } from "react";

import { ConfigContext } from "@context";

import { ConfigurationWrapper, Configuration, ConfigurationItem } from "@local";

const ZIndex = () => {
  return (
    <Fragment>
      <ConfigContext.Consumer>
        {(context) => {
          const { config, updateConfig, updateSavedItems } = context;
          const { zIndex } = config;
          return (
            <ConfigurationWrapper
              configValue={zIndex}
              title="Z-index"
              configMapKey="zIndex"
              updateConfig={updateConfig}
              updateSavedItems={updateSavedItems}
            >
              <Configuration>
                <ConfigurationItem
                  namePlaceholder="low"
                  valuePlaceholder="10"
                  configMapKey="zIndex"
                  updateSavedItems={updateSavedItems}
                />
              </Configuration>
            </ConfigurationWrapper>
          );
        }}
      </ConfigContext.Consumer>
    </Fragment>
  );
};

export default ZIndex;
