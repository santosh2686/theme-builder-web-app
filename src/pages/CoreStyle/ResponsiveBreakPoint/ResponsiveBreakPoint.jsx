import React, { Fragment } from "react";

import { ConfigContext } from "@context";

import { ConfigurationWrapper, Configuration, ConfigurationItem } from "@local";

const ResponsiveBreakPoint = () => {
  return (
    <Fragment>
      <ConfigContext.Consumer>
        {(context) => {
          const { config, updateConfig, updateSavedItems } = context;
          const { breakpoint } = config;
          return (
            <ConfigurationWrapper
              configValue={breakpoint}
              title="Responsive break point"
              configMapKey="breakpoint"
              updateConfig={updateConfig}
              updateSavedItems={updateSavedItems}
            >
              <Configuration>
                <ConfigurationItem
                  namePlaceholder="default"
                  valuePlaceholder="320px"
                  configMapKey="breakpoint"
                  updateSavedItems={updateSavedItems}
                />
              </Configuration>
            </ConfigurationWrapper>
          );
        }}
      </ConfigContext.Consumer>
    </Fragment>
  );
};

export default ResponsiveBreakPoint;
