import React, { Fragment } from "react";

import { ConfigContext } from "@context";

import { ConfigurationWrapper, Configuration } from "@local";

import TypographyConfiguration from "./TypographyConfiguration/TypographyConfiguration.jsx";
import TypographyOutput from "./TypographyOutput/TypographyOutput.jsx";

const Typography = () => {
  return (
    <Fragment>
      <ConfigContext.Consumer>
        {(context) => {
          const { config, updateConfig, updateSavedItems } = context;
          const { typography } = config;
          return (
            <ConfigurationWrapper
              configValue={typography}
              title="Typography"
              configMapKey="typography"
              updateConfig={updateConfig}
              updateSavedItems={updateSavedItems}
            >
              <Configuration withOutput>
                <TypographyConfiguration
                  configMapKey="typography"
                  updateSavedItems={updateSavedItems}
                />
                <TypographyOutput />
              </Configuration>
            </ConfigurationWrapper>
          );
        }}
      </ConfigContext.Consumer>
    </Fragment>
  );
};

export default Typography;
