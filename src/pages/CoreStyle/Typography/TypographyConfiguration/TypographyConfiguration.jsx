import React, { PureComponent } from "react";

import { Row, Col, TextInput } from "@common";

class TypographyConfiguration extends PureComponent {
  changeHandler = (data) => {
    const { changeHandler, updateSavedItems, configMapKey, index } = this.props;
    changeHandler({
      [index]: {
        $merge: {
          ...data,
          value: {}
        },
      },
    });
    updateSavedItems(configMapKey, false);
  };

  valueChangeHandler = (data) => {
    const { changeHandler, updateSavedItems, configMapKey, index } = this.props;
    changeHandler({
      [index]: {
        value: {
            $merge: data,
        }
      },
    });
    updateSavedItems(configMapKey, false);
  }

  render() {
    const { item } = this.props;
    const { name, value } = item;
    const { fontSize, letterSpacing, lineHeight } = value;
    return (
      <Row>
        <Col col={{ xs: 12, sm: 12, md: 12, lg: 6 }}>
          <TextInput
            label="Name"
            name="name"
            value={name}
            classes="mar-b-10"
            changeHandler={this.changeHandler}
            attributes={{
              placeholder: 'e.g. xxxl'
            }}
          />
          <TextInput
            label="Letter spacing"
            name="letterSpacing"
            value={letterSpacing}
            disabled={!name}
            changeHandler={this.valueChangeHandler}
            attributes={{
              placeholder: 'e.g. 16px'
            }}
          />
        </Col>
        <Col col={{ xs: 12, sm: 12, md: 12, lg: 6 }}>
          <TextInput
            label="Font size"
            name="fontSize"
            value={fontSize}
            disabled={!name}
            classes="mar-b-10"
            changeHandler={this.valueChangeHandler}
            attributes={{
              placeholder: 'e.g. 16px'
            }}
          />
          <TextInput
            label="Line height"
            name="lineHeight"
            value={lineHeight}
            disabled={!name}
            changeHandler={this.valueChangeHandler}
            attributes={{
              placeholder: 'e.g. 16px'
            }}
          />
        </Col>
      </Row>
    );
  }
}

export default TypographyConfiguration;
