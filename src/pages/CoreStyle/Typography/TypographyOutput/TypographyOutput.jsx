import React, { PureComponent, Fragment } from "react";
import { MapCssModules } from "@utils";

class TypographyOutput extends PureComponent {
  render() {
    const { data } = this.props;
    return (
      <Fragment>
        {data.map((item) => {
          const { name, value } = item;
          const { fontSize, letterSpacing, lineHeight } = value;
          return (
            <div key={name} styleName="pad-tb-20 bor-b-gray-light">
              <div
                style={{
                  fontSize,
                  letterSpacing,
                  lineHeight,
                }}
              >
                E.g.: The quick brown for jumps over the lazy dog.
              </div>
              <div styleName="color-gray pad-t-5">
                name: {name} &nbsp; &nbsp; font-size: {fontSize} &nbsp; &nbsp;
                letter-spacing: {letterSpacing} &nbsp; &nbsp; line-height:{" "}
                {lineHeight}
              </div>
            </div>
          );
        })}
      </Fragment>
    );
  }
}

export default MapCssModules(TypographyOutput);
