import React, { Fragment } from "react";

import { ConfigContext } from "@context";

import { ConfigurationWrapper, Configuration, ConfigurationItem } from "@local";

const FontWeight = () => {
  return (
    <Fragment>
      <ConfigContext.Consumer>
        {(context) => {
          const { config, updateConfig, updateSavedItems } = context;
          const { fontWeight } = config;
          return (
            <ConfigurationWrapper
              configValue={fontWeight}
              title="Font weight"
              configMapKey="fontWeight"
              updateConfig={updateConfig}
              updateSavedItems={updateSavedItems}
            >
              <Configuration>
                <ConfigurationItem
                  namePlaceholder="regular"
                  valuePlaceholder="#400"
                  configMapKey="fontWeight"
                  updateSavedItems={updateSavedItems}
                />
              </Configuration>
            </ConfigurationWrapper>
          );
        }}
      </ConfigContext.Consumer>
    </Fragment>
  );
};

export default FontWeight;
