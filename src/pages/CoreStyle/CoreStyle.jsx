import React from "react";
import { shape, string } from "prop-types";
import { Route, Switch, Redirect } from "react-router-dom";

import Container from './Container/Container.jsx';
import Typography from './Typography/Typography.jsx';
import FontWeight from './FontWeight/FontWeight.jsx';
import ZIndex from './ZIndex/ZIndex.jsx';
import Spacing from './Spacing/Spacing.jsx';
import ResponsiveBreakPoint from './ResponsiveBreakPoint/ResponsiveBreakPoint.jsx';

const CoreStyle = ({
    match: { url }
}) => {
  return (
    <Switch>
      <Route exact path={url} render={() => <Redirect to={`${url}/container`} />} />
      <Route path={`${url}/container`} component={Container} />
      <Route path={`${url}/typography`} component={Typography} />
      <Route path={`${url}/font-weight`} component={FontWeight} />
      <Route path={`${url}/z-index`} component={ZIndex} />
      <Route path={`${url}/spacing`} component={Spacing} />
      <Route path={`${url}/responsive-breakpoint`} component={ResponsiveBreakPoint} />
      <Route render={() => <Redirect to={`${url}/container`} />} />
    </Switch>
  );
};

CoreStyle.prototypes = {
    match: shape({
        url: string
    })
};

CoreStyle.defaultProps = {
    match: {
        url: '',
    }
};

export default CoreStyle
