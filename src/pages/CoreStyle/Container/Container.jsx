import React, { Fragment } from "react";

import { ConfigContext } from "@context";

import { ConfigurationWrapper } from "@local";

import ContainerConfiguration from "./ContainerConfiguration/ContainerConfiguration.jsx";

const Container = () => {
  return (
    <Fragment>
      <ConfigContext.Consumer>
        {(context) => {
          const { config, updateConfig, updateSavedItems } = context;
          const { container } = config;
          return (
            <ConfigurationWrapper
              configValue={container}
              title="Container"
              configMapKey="container"
              updateConfig={updateConfig}
              updateSavedItems={updateSavedItems}
              keepOriginal
            >
              <ContainerConfiguration
                configMapKey="container"
                updateSavedItems={updateSavedItems}
              />
            </ConfigurationWrapper>
          );
        }}
      </ConfigContext.Consumer>
    </Fragment>
  );
};

export default Container;
