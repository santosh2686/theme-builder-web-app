import React, { PureComponent } from "react";

import { Panel, Row, Col, TextInput } from "@common";

class ContainerConfiguration extends PureComponent {
  changeHandler = (data) => {
    const { changeHandler, updateSavedItems, configMapKey } = this.props;
    changeHandler({
        $merge: data
    });
    updateSavedItems(configMapKey, false);
  };

  render() {
    const { data } = this.props;
    const { minWidth, maxWidth } = data;
    return (
      <Panel title="Configuration">
        <Row>
          <Col col={{ xs: 12, sm: 12, md: 6, lg: 4 }} classes="pad-b-10">
            <TextInput
              name="minWidth"
              label="Width"
              value={minWidth}
              changeHandler={this.changeHandler}
              attributes={{
                placeholder: "e.g. auto, 90%, 100px",
              }}
            />
          </Col>
          <Col col={{ xs: 12, sm: 12, md: 6, lg: 4 }} classes="pad-b-10">
            <TextInput
              name="maxWidth"
              label="Max width"
              value={maxWidth}
              changeHandler={this.changeHandler}
              attributes={{
                placeholder: "e.g. auto, 90%, 100px",
              }}
            />
          </Col>
        </Row>
      </Panel>
    );
  }
}

export default ContainerConfiguration;
