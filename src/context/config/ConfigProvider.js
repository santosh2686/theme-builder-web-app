import React, { PureComponent } from "react";
import update from "immutability-helper";
import ConfigContext from "./ConfigContext.js";

import appConfig from './appConfig'

class ConfigProvider extends PureComponent {
  state = {
    config: appConfig,
    savedItems: {},
  };

  updateConfig = (config) => {
    this.setState((prevState) =>
      update(prevState, {
        config,
      })
    );
  };

  updateSavedItems = (config, flag) => {
    this.setState((prevState) =>
      update(prevState, {
        savedItems: {
          $merge: {
            [config]: flag,
          },
        },
      })
    );
  };

  getConfigValue = (configKey, configProp) => {
    const { config } = this.state;
    const { baseColors, brandColors } = config;
    const colorOptions = {
      ...baseColors,
      ...brandColors,
    }
    return (configKey === 'colors') ? colorOptions[configProp] : config[configKey][configProp]
  }

  render() {
    const { children } = this.props;
    const { config, savedItems } = this.state;
    return (
      <ConfigContext.Provider
        value={{
          config,
          savedItems,
          updateConfig: this.updateConfig,
          updateSavedItems: this.updateSavedItems,
          getConfigValue: this.getConfigValue
        }}
      >
        {children}
      </ConfigContext.Provider>
    );
  }
}

export default ConfigProvider;
