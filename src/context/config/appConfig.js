const config = {
  baseColors: {
    black: "#000000",
    "gray-darker": "#555556",
    "gray-dark": "#666666",
    gray: "#878787",
    "gray-light": "#dedede",
    "gray-lighter": "#f7f7f9",
    white: "#FFFFFF",
  },
  brandColors: {
    primary: "#4e9de6",
    success: "#2ecd99",
    error: "#ed6f56",
    warning: "#f0c541",
    info: "#5bc0de",
  },
  transparentBackground: {
    low: "rgba(0, 0, 0, 0.1)",
    medium: "rgba(0, 0, 61, 0.3)",
    high: "rgba(0, 0, 0, 0.5)",
  },
  container: {
    minWidth: "90%",
    maxWidth: "1600px",
  },
  typography: {
    xs: {
      fontSize: "12px",
      letterSpacing: "0.1px",
      lineHeight: "16px",
    },
    s: {
      fontSize: "14px",
      letterSpacing: "0.2px",
      lineHeight: "16px",
    },
    m: {
      fontSize: "16px",
      letterSpacing: "0.2px",
      lineHeight: "20px",
    },
    l: {
      fontSize: "18px",
      letterSpacing: "0.2px",
      lineHeight: "24px",
    },
    xl: {
      fontSize: "22px",
      letterSpacing: "0.3px",
      lineHeight: "28px",
    },
    xxl: {
      fontSize: "28px",
      letterSpacing: "0.3px",
      lineHeight: "36px",
    },
    xxxl: {
      fontSize: "36px",
      letterSpacing: "0.6px",
      lineHeight: "48px",
    },
  },
  headings: {
    h1: 'xxl',
    h2: 'xl',
    h3: 'l',
    h4: 'm',
    h5: 's',
    h6: 'xs'
  },
  spacing: {
    4: "4px",
    8: "8px",
    12: "12px",
    16: "16px",
  },
  fontWeight: {
    regular: "400",
    semiBold: "600",
    bold: "700",
  },
  zIndex: {
    "very-low": "1",
    low: "10",
    medium: "50",
    high: "70",
    "very-high": "90",
  },
  breakpoint: {
    default: "320px",
    small: "480px",
    medium: "768px",
    large: "1024px",
  },
  pageBody: {
    backgroundColor: "white"
  },
  defaultTypography: {
    fontFamily: "Helvetica",
    typography: "m",
    fontWeight: "regular"
  },
  textSelection: {
    backgroundColor: "primary",
    textColor: "white",
  },
  components: {
    grid: {},
    table: {
      common: {
        horizontalBorderWidth: '',
        horizontalBorderColor: '',
        verticalBorderWidth: '',
        verticalBorderColor: ''
      },
      header: {
        backgroundColor: '',
        textColor: '',
      },
      body: {
        backgroundColor: '',
        typography: '',
        fontWeight: '',
      },
    },
    container: {},
    button: {
      common: {
        borderRadius: "4px",
        shadowColorOnHover: 'gray-light',
        disabledOpacity: 0.5
      },
      category: {
        primary: {
          basic: {
            backgroundColor: "primary",
            textColor: "white",
            loaderBackground: "white",
            hover: {
              backgroundColor: "primary",
              textColor: "white"
            }
          },
          outline: {
            backgroundColor: "white",
            textColor: "primary",
            borderWidth: "1px",
            borderColor: "primary",
            loaderBackground: "primary",
            hover: {
              backgroundColor: "white",
              textColor: "primary",
              borderWidth: "2px",
              borderColor: "primary",
            }
          }
        },
        success: {
          basic: {
            backgroundColor: "success",
            textColor: "white",
            loaderBackground: "gray",
            hover: {
              backgroundColor: "success",
              textColor: "white"
            }
          },
          outline: {
            backgroundColor: "white",
            textColor: "success",
            borderWidth: "1px",
            borderColor: "success",
            loaderBackground: "success",
            hover: {
              backgroundColor: "white",
              textColor: "success",
              borderWidth: "2px",
              borderColor: "success",
            }
          }
        }
      },
      size: {
        default: {
          height: "40px",
          horizontalSpacing: "25px",
          typography: "m",
          fontWeight: "regular"
        }
      },
    },
    radioButton: {
      label: {
        "typography": "m",
        "textColor": "gray",
        "leftSpacing": "4",
        "fontWeight": "regular"
      },
      default: {
        "borderWidth": "1px",
        "borderColor": "success",
        "backgroundColor": "white"
      },
      hover: {
        "borderWidth": "2px",
        "borderColor": "success",
        "backgroundColor": "white"
      },
      selected: {
        "borderWidth": "2px",
        "borderColor": "success",
        "backgroundColor": "white",
        "fillColor": "success"
      },
      disabled: {
        "borderWidth": "1px",
        "borderColor": "gray-light",
        "backgroundColor": "gray-light",
        "fillColor": "white"
      },
    },
    checkBox: {
      label: {
        "typography": "m",
        "textColor": "gray",
        "leftSpacing": "4",
        "fontWeight": "regular"
      },
      common: {
        "borderRadius": "4px",
      },
      default: {
        "borderWidth": "1px",
        "borderColor": "success",
        "backgroundColor": "white"
      },
      hover: {
        "borderWidth": "2px",
        "borderColor": "success",
        "backgroundColor": "white"
      },
      selected: {
        "borderWidth": "2px",
        "borderColor": "success",
        "backgroundColor": "success",
        "tickColor": "white"
      },
      disabled: {
        "borderWidth": "1px",
        "borderColor": "gray-light",
        "backgroundColor": "gray-light",
        "tickColor": "white"
      },
    },
    toggleButton: {
      "common": {
        "disabledOpacity": 0.5
      },
      "track": {
        "height": "24px",
        "width": "52px",
        "borderRadius": "12px",
        "backgroundColor": "gray-light",
        "activeBackgroundColor": "primary"
      },
      "bubble": {
        "height": "16px",
        "width": "16px",
        "borderRadius": "50%",
        "backgroundColor": "white",
        "activeBackgroundColor": "white"
      },
      "label": {
        "typography": "m",
        "textColor": "gray",
        "leftSpacing": "8"
      }
    },
    inputField: {
      "common": {
        "borderRadius": "4px",
        "shadowColorOnHover": "gray-light",
        "disabledOpacity": 0.5,
        "height": "40px",
        "verticalSpacing": "8",
        "horizontalSpacing": "16"
      },
      "label": {
        "typography": "m",
        "textColor": "gray",
        "bottomSpacing": "4",
        "requiredStarColor": "error",
        "fontWeight": "regular"
      },
      "default": {
        "borderWidth": "1px",
        "borderColor": "gray",
        "textColor": "black",
        "placeHolderTextColor": "gray"
      },
      "hover": {
        "borderWidth": "1px",
        "borderColor": "black",
        "textColor": "black"
      },
      "focus": {
        "borderWidth": "1px",
        "borderColor": "black",
        "textColor": "black"
      },
      "valid": {
        "borderWidth": "1px",
        "borderColor": "success",
        "textColor": "black"
      },
      "invalid": {
        "borderWidth": "1px",
        "borderColor": "error",
        "textColor": "black"
      }
    },
    dropDown: {
      "common": {
        "borderRadius": "4px",
        "shadowColorOnHover": "gray-light",
        "disabledOpacity": 0.5,
        "height": "40px",
        "verticalSpacing": "8",
        "horizontalSpacing": "16"
      },
      "label": {
        "typography": "m",
        "textColor": "gray",
        "bottomSpacing": "4",
        "requiredStarColor": "error",
        "fontWeight": "regular"
      },
      "placeholder": {
        "typography": "m",
        "textColor": "gray",
        "fontWeight": "regular"
      },
      "arrow": {
        "color": "black",
      },
      "default": {
        "borderWidth": "1px",
        "borderColor": "gray",
        "textColor": "black",
        "placeHolderTextColor": "gray"
      },
      "hover": {
        "borderWidth": "1px",
        "borderColor": "black",
        "textColor": "black"
      },
      "focus": {
        "borderWidth": "1px",
        "borderColor": "black",
        "textColor": "black"
      },
      "valid": {
        "borderWidth": "1px",
        "borderColor": "success",
        "textColor": "black"
      },
      "invalid": {
        "borderWidth": "1px",
        "borderColor": "error",
        "textColor": "black"
      }
    },
    fileUpload: {},
    rangeSlider: {},
    anchor: {
      "category": {
        "primary": {
          "default": {
            "typography": "m",
            "fontWeight": "regular",
            "textDecoration": "none",
            "color": "primary"
          },
          "hover": {
            "textDecoration": "underline",
            "color": "primary"
          },
          "disabled": {
            "color": "gray"
          }
        },
        "secondary": {
          "default": {
            "typography": "m",
            "fontWeight": "regular",
            "textDecoration": "underline",
            "color": "gray"
          },
          "hover": {
            "textDecoration": "underline",
            "color": "gray"
          },
          "disabled": {
            "color": "gray-light"
          }
        }
      },
      "standAlone": {
        "verticalSpacing": "8",
        "horizontalSpacing": "16"
      }
    },
    tab: {
      "common": {
        "maxNoOfTabs": "5",
        "typography": "m",
        "fontWeight": "regular",
        "textColor": "gray-dark",
        "backgroundColor":  "white",
        "bottomBorderColor": "gray-light",
        "bottomBorderWidth": "1px",
        "verticalSpacing": "16",
        "horizontalSpacing": "16"
      },
      "hover": {
        "backgroundColor":  "gray-lighter",
        "textColor": "black"
      },
      "active": {
        "backgroundColor":  "white",
        "textColor": "black",
        "bottomBorderColor": "success",
        "bottomBorderWidth": "4px",
        "fontWeight": "regular"
      },
      "content": {
        "verticalSpacing": "16",
        "horizontalSpacing": "16"
      }
    },
    stepper: {
      divider: {},
      completedDivider: {},
      step: {},
      stepLabel: {},
      activeStep: {},
      activeStepLabel: {},
      completedStep: {},
      completedStepLabel: {},
    },
    treeView: {},
    breadCrumb: {},
    listView: {},
    pagination: {},
    alert: {
      common: {
        horizontalSpacing: "4",
        verticalSpacing: "4",
        borderWidth: "1px",
      },
      category: {
        primary: {
          backgroundColor: "white",
          borderColor: "gray-faded",
          textColor: 'black',
          closeButtonColor: 'black'
        }
      },
      closeButton: {
        height: "28px",
        width: "28px"
      }
    },
    badge: {
      common: {
        minHeight: '20px',
        minWidth: '20px',
        borderRadius: '50%',
        typography: 'm',
        fontWeight: 'regular'
      },
      category: {
        primary: {
          backgroundColor: 'gray',
          textColor: 'black'
        },
        secondary: {
          backgroundColor: 'success',
          textColor: 'white'
        }
      },
    },
    progressBar: {
      "backgroundColor": "default",
      "foregroundColor": "primary",
      "labels": {
        "typography": "m",
        "fontWeight": "semiBold"
      }
    },
    modal: {
      "overlay": {
        "transparentBackground": "medium"
      },
      "header": {
        "backgroundColor": "black",
        "horizontalSpacing": "16",
        "verticalSpacing": "16",
        "borderBottomColor": "gray-light",
        "borderBottomWidth": "1px",
        "typography": "m",
        "fontWeight": "regular",
        "textColor": "white"
      },
      "closeButton": {
        "color": "white",
        "height": "30px",
        "width": "30px"
      },
      "closeHover": {
        "backgroundColor": "white",
        "color": "white"
      },
      "body": {
        "horizontalSpacing": "16",
        "verticalSpacing": "16"
      },
      "footer": {
        "backgroundColor": "gray-fader",
        "horizontalSpacing": "16",
        "verticalSpacing": "16",
        "borderTopColor": "gray-light",
        "borderTopWidth": "1px"
      }
    },
    accordion: {},
    chip: {
      common: {
        "horizontalSpacing": "8",
        "verticalSpacing": "8",
        "typography": "s",
        "fontWeight": "regular",
      },
      category: {
        primary: {
          "backgroundColor": "gray",
          "textColor": "gray",
          closeButtonColor: 'gray'
        }
      },
      "closeButton": {
        "height": "8px",
        "width": "8px"
      }
    },
    spinner: {
      "common": {
        "borderWidth": "2px"
      },
      "category": {
        "primary": {
          "backgroundColor": "primary",
          "foregroundColor": "white"
        }
      },
      "size": {
        "default": {
          "height": "40px",
          "width": "40px"
        }
      }
    },
    tooltip: {
      common: {
        spacing: "8",
        typography: "m",
        borderWidth: "1px"
      },
      category: {
        primary: {
          backgroundColor: "black",
          textColor: "white",
          borderColor: "black"
        },
        secondary: {
          backgroundColor: "white",
          textColor: "black",
          borderColor: "gray"
        }
      }
    },
    popOver: {
      "common": {
        "spacing": "8",
        "typography": "m",
        "borderWidth": "1px"
      },
      "header": {
        "backgroundColor": "gray-light",
        "textColor": "black",
        "horizontalSpacing": "4",
        "verticalSpacing": "4",
      },
      closeButton: {
        "color": "gray-lightest",
        "height": "28px",
        "width": "28px"
      },
      "container": {
        "spacing": "8"
      }
    },
    collapsible: {
      "common": {
        "borderRadius": "4px",
        "borderWidth": "1px",
        "borderColor": "gray-lightest",
        "shadowColorOnHover": "gray-light"
      },
      "header": {
        "backgroundColor": "gray-lighter",
        "horizontalSpacing": "16",
        "height": "52px"
      },
      "headerTitle": {
        "typography": "l",
        "fontWeight": "regular",
        "textColor": "gray-normal"
      },
      "container": {
        "verticalSpacing": "16",
        "horizontalSpacing": "16",
        "backgroundColor": "white"
      }
    },
  }
}

export default config
