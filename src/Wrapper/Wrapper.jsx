import React, { PureComponent } from 'react';
import { BrowserRouter, Route, Switch, Redirect } from 'react-router-dom';
import ClassNames from 'classnames';

import { MapCssModules } from '@utils';
import { Navigation, Header } from '@local';
import { ConfigProvider } from '@context';

import DashBoard from '../pages/DashBoard/DashBoard.jsx';

import CoreStyle from '../pages/CoreStyle/CoreStyle.jsx';
import ColorSystem from '../pages/ColorSystem/ColorSystem.jsx';
import GlobalSettings from '../pages/GlobalSettings/GlobalSettings.jsx';
import LayoutElements from "../pages/Components/LayoutElements/LayoutElements"
import FormElements from "../pages/Components/FormElements/FormElements"
import NavigationalElements from "../pages/Components/NavigationalElements/NavigationalElements"
import NotificationElements from "../pages/Components/NotificationElements/NotificationElements"
import PresentationalElements from "../pages/Components/PresentationalElements/PresentationalElements"

class Wrapper extends PureComponent {
  state = {
    isToggle: false,
    selection: false,
  };

  onSelection = () => {
    this.setState({
      selection: true
    })
  }

  toggleNavigation = () => {
    this.setState(({ isToggle }) => ({ isToggle: !isToggle }));
  };

  render() {
    const { isToggle, selection } = this.state;
    const toggleClass = ClassNames({
      "side-nav-toggle": isToggle,
    });

    if(!selection) {
      return (
        <DashBoard onSelection={this.onSelection} />
      )
    }

    return (
      <BrowserRouter>
        <ConfigProvider>
          <Header classes={toggleClass} clickHandler={this.toggleNavigation} />
          <div styleName="flex flex-1 overflow-hidden">
            <Navigation classes={toggleClass} />
            <div styleName="flex flex-column flex-1 overflow-hidden bor-t-gray-light">
              <div styleName="flex-1 overflow-auto pad-20 flex flex-column">
                <Switch>
                  <Route exact path="/" render={() => <Redirect to="/color-system" />} />
                  <Route path="/core-style" component={CoreStyle} />
                  <Route path="/color-system" component={ColorSystem} />
                  <Route path="/global" component={GlobalSettings} />
                  <Route path="/layout-components" component={LayoutElements} />
                  <Route path="/form-components" component={FormElements} />
                  <Route path="/navigational-components" component={NavigationalElements} />
                  <Route path="/notification-components" component={NotificationElements} />
                  <Route path="/presentational-components" component={PresentationalElements} />
                  <Route render={() => <Redirect to="/color-system" />} />
                </Switch>
              </div>
              <div styleName="pad-lr-20 pad-tb-10 bg-white bor-t-gray-light text-right font-12">
                Copyright © 2020 ThemeBuilder. All rights reserved.
              </div>
            </div>
          </div>
        </ConfigProvider>
      </BrowserRouter>
    );
  }
}

export default MapCssModules(Wrapper);
