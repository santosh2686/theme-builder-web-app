import React from 'react'
import { MapCssModules } from '@utils';
import { Layout } from '@common'

const CheckBox = ({ styleData, label, labelStyle, tickStyle }) => (
  <Layout flex={{ align: 'center' }}>
    <div styleName="demo-check-box" style={styleData}>
      {tickStyle && (
        <span style={tickStyle} />
      )}
    </div>
    <span style={labelStyle}>
      {label}
    </span>
  </Layout>
)

export default MapCssModules(CheckBox)
