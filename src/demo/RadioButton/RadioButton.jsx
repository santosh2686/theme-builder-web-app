import React from 'react'
import { MapCssModules } from '@utils';
import { Layout } from '@common'

const RadioButton = ({ styleData, label, labelStyle, fillStyle }) => (
  <Layout flex={{ align: 'center' }}>
    <div styleName="demo-radio-button" style={styleData}>
      {fillStyle && (
        <span style={fillStyle} />
      )}
    </div>
    <span style={labelStyle}>
      {label}
    </span>
  </Layout>
)

export default MapCssModules(RadioButton)
