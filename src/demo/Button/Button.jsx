import React from "react";
import { MapCssModules } from '@utils';

const Button = ({ data, isLoading }) => {
  return(
    <button styleName="demo-btn" style={data}>
      {isLoading && <span styleName="demo-btn-loader" style={{
        borderColor: data.loaderBg
      }} /> }
      Button
    </button>
  )
}

export default MapCssModules(Button)
