const routes = [
  {
    label: 'Color palette',
    icon: 'table',
    active: 'color-system',
    items: [{
      label: 'Base colors',
      route: '/color-system/base-color'
    }, {
      label: 'Brand colors',
      route: '/color-system/brand-color'
    }, {
      label: 'Transparent',
      route: '/color-system/transparent'
    }]
  },
  {
    label: 'Base Configuration',
    icon: 'diamond',
    active: 'core-style',
    items: [
      {
        label: 'Container',
        route: '/core-style/container'
      },
      {
        label: 'Typography',
        route: '/core-style/typography'
      },
      {
        label: 'Font weight',
        route: '/core-style/font-weight'
      },
      {
        label: 'z-index',
        route: '/core-style/z-index'
      },
      {
        label: 'Spacing',
        route: '/core-style/spacing'
      },
      {
        label: 'Responsive breakpoints',
        route: '/core-style/responsive-breakpoint'
      }]
  },
  {
    label: 'Global settings',
    icon: 'globe',
    active: 'global',
    items: [{
      label: 'Page body',
      route: '/global/page-body'
    },
      {
        label: 'Default font details',
        route: '/global/default-typography'
      },
      {
        label: 'Headings',
        route: '/global/headings'
      },
      {
        label: 'Text selection',
        route: '/global/text-selection'
      }]
  },
  {
    label: 'Layout',
    icon: 'th',
    active: 'layout-components',
    items: [
      {
        label: 'Container',
        route: '/layout-components/container'
      },
      {
        label: 'Grid',
        route: '/layout-components/grid'
      },
      {
        label: 'Table',
        route: '/layout-components/table'
      }
    ]
  },
  {
    label: 'Form elements',
    icon: 'code',
    active: 'form-components',
    items: [
      {
        label: 'Button',
        route: '/form-components/button'
      },
      {
        label: 'Radio button',
        route: '/form-components/radio-button'
      },
      {
        label: 'Checkbox',
        route: '/form-components/check-box'
      },
      {
        label: 'Toggle button',
        route: '/form-components/toggle-button'
      },
      {
        label: 'Text input',
        route: '/form-components/text-input'
      },
      {
        label: 'Dropdown',
        route: '/form-components/drop-down'
      },
      {
        label: 'File upload',
        route: '/form-components/file-upload'
      },
      {
        label: 'Range slider',
        route: '/form-components/range-slider'
      },
    ]
  },
  {
    label: 'Navigational',
    icon: 'external-link',
    active: 'navigational-components',
    items: [
      {
        label: 'Anchor',
        route: '/navigational-components/anchor'
      },
      {
        label: 'Tab',
        route: '/navigational-components/tabs'
      },
      {
        label: 'Stepper',
        route: '/navigational-components/stepper'
      },
      {
        label: 'Tree view',
        route: '/navigational-components/tree-view'
      },
      {
        label: 'Breadcrumb',
        route: '/navigational-components/bread-crumb'
      },
      {
        label: 'List view',
        route: '/navigational-components/list-view'
      },
      {
        label: 'Pagination',
        route: '/navigational-components/pagination'
      }
    ]
  },
  {
    label: 'Notification',
    icon: 'exclamation-triangle',
    active: 'notification-components',
    items: [
      {
        label: 'Alert',
        route: '/notification-components/alert'
      },
      {
        label: 'Badge',
        route: '/notification-components/badge'
      },
      {
        label: 'Progress bar',
        route: '/notification-components/progress-bar'
      },
      {
        label: 'Modal',
        route: '/notification-components/modal'
      }
    ]
  },
  {
    label: 'Presentational',
    icon: 'television',
    active: 'presentational-components',
    items: [
      {
        label: 'Accordion',
        route: '/presentational-components/accordion'
      },
      {
        label: 'Chip',
        route: '/presentational-components/chip'
      },
      {
        label: 'Spinner',
        route: '/presentational-components/spinner'
      },
      {
        label: 'Tooltip',
        route: '/presentational-components/tooltip'
      },
      {
        label: 'Popover',
        route: '/presentational-components/pop-over'
      },
      {
        label: 'Collapsible',
        route: '/presentational-components/collapsible'
      }
    ]
  }
];

export default routes
