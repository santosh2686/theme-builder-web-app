import CSSModules from 'react-css-modules';
import ClassNames from 'classnames';
import Style from '../app.scss';

const MapCssModules = (Component) => {
  return CSSModules(Component, Style, {
    allowMultiple: true,
    handleNotFoundStyleName: 'ignore'
  })
};

const isEmpty = (value) => {
  return value === undefined || value === null || value === "";
};

const convertObjToQueryString = (obj) => {
  return Object.keys(obj).reduce((acc, next) => {
    acc = acc ? `${acc}&${next}=${obj[next]}` : `${next}=${obj[next]}`;
    return acc
  }, '')
};

const formatMarginPaddingClass = (input, cssProp) => {
  if(!input) {
    return '';
  }
  const lookUp = { lg: true, md: true, sm: true, xs: true };
  const parent = 'xs';
  let result = [];
  function calculateClass(inputObj, prefix) {
    if(typeof inputObj !== 'object') {
      if(prefix === 'xs') {
        return [`${cssProp}-${inputObj}`];
      }
      return [`${cssProp}-${prefix}-${inputObj}`];
    }
    return compute(inputObj, prefix);
  }
  function compute(obj, pr) {
    return Object.keys(obj).reduce((acc, next) => {
      if(lookUp[next]) {
        acc = acc.concat(calculateClass(obj[next], next));
      } else {
        const formClass = (pr === 'xs') ? `${cssProp}-${next}-${obj[next]}` : `${cssProp}-${pr}-${next}-${obj[next]}`;
        acc.push(formClass);
      }
      return acc
    }, [])
  }
  result = result.concat(calculateClass(input, parent));
  return result.join(' ')
};

const formatBreakPointClasses = (input, prefix) => {
  if(!input) {
    return ''
  }
  if(typeof input !== 'object') {
    return `${prefix}-${input}`
  }

  return Object.keys(input).reduce((acc, next) => {
    if(next === 'xs') {
      acc = `${acc} ${prefix}-${input[next]}`;
    } else {
      acc = `${acc} ${prefix}-${next}-${input[next]}`;
    }
    return acc;
  }, '');
};

const formatFlexClasses = (input) => {
  if(!input) {
    return ''
  }
  const { direction, reverse, align, justify, wrap, wrapReverse } = input;
  return ClassNames('flex', {
    'flex-wrap': wrap && !wrapReverse,
    'flex-wrap-reverse': wrap && wrapReverse,
    [`flex-${direction}`]: direction && !reverse,
    [`flex-${direction}-reverse`]: direction && reverse,
    [`align-${align}`]: align,
    [`flex-${justify}`]: justify,
  });
};

const computeValue = (obj, map) => {
  return !isEmpty(map) && map.split('.').reduce((a, b) => {
    return (a && !isEmpty(a[b])) ? a[b] : '';
  }, obj);
};

const objectToArray = obj => Object.keys(obj).reduce((acc, next) => {
  acc.push({
    name: next,
    value: obj[next]
  })
  return acc;
}, [])

const arrayToObject = arr => arr.reduce((acc, next) => {
  const { name, value } = next;
  if(name) {
    acc[name] = value;
  }
  return acc
}, {})

const removeUnit = (str) => {
  const value = str.substring(0, str.length - 2);
  return Number(value)
};

export {
  MapCssModules,
  formatMarginPaddingClass,
  formatBreakPointClasses,
  formatFlexClasses,
  computeValue,
  convertObjToQueryString,
  objectToArray,
  arrayToObject,
  removeUnit,
}
